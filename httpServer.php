<?php

use must\develop\Debugger;
use must\develop\Monitor;
use must\Launch;

const ROOT = __DIR__;
require ROOT.'/must/Autoload.php';
require ROOT.'/must/develop/debug.php';

$server = (function(){
    $isCLi = Debugger::isRunInCLI();
    if(false === $isCLi )
    {
        say('不是cli');
        exit;
    }
    unset($isCLi);
    //检查扩展
    echo  true === extension_loaded('redis') ? '赞，已经开启redis扩展'.PHP_EOL :
        '未发现redis扩展---然鹅，管理 $_SESSION 需要连接 redis',
        '（ $_SESSION的原生行为在swoole的http server中不起作用，',
        '所以需要接管session ）'.PHP_EOL.PHP_EOL;
    //
    /*Http\Server对Http协议的支持并不完整，建议仅作为应用服务器。并且在前端增加Nginx作为代理*/
    $launch = new Launch();
    $server = $launch->initializeServer(true);

    $masterObj = new \must\Master();
    $server->on('start', [ $masterObj, 'onStart']);

    $managerObj = new \must\Manager();
    $server->on('managerStart', [ $managerObj, 'onManagerStart']);
    $server->on('managerStop', [ $managerObj, 'onManagerStop']);
    $server->on('afterReload', [ $managerObj, 'onAfterReload']);

    $workerObj = new \must\WorkersCommon();
    $server->on('workerStart', [ $workerObj, 'onWorkerStart']);
    $server->on('workerStop', [ $workerObj, 'onWorkerStop']);
    $server->on('workerError', [ $workerObj, 'onWorkerError']);

    $mainObj = new \must\MainWorkers();
    $server->on('request', [ $mainObj, 'onRequest' ]);
    $server->on('finish',  [ $mainObj, 'onFinish']);
    $server->on('pipeMessage', [ $mainObj, 'onPipeMessage']);

    $taskObj = new \must\TaskWorkers();
    $server->on('task', [ $taskObj,  'onTask']);

    return $server;
})();
//热更新
Monitor::watchByProcess([
    Launch::APP_COMMON_PATH,
    APP_PATH.'/api',
    APP_PATH.'/config',
    APP_PATH.'/function',
]);
//启动
echo Debugger::cliSetColor('启动 Swoole http server...','green').PHP_EOL.PHP_EOL;
//
$server->start();
