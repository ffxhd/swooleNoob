<?php
namespace app_common;
use app_common\controller\Base;
use app_common\controller\DbBusiness;
use app_common\controller\HttpRequest;
use app_common\core\session\SessionFactory;
use app_common\core\wx\miniApp\MiniApp;
use JetBrains\PhpStorm\Pure;
use must\develop\Debugger;

class PC{

    public  static function analysisUri(&$requestUri): string|array
    {
        /*swoole test.swoole.local:9501/test/index?field=val&field2=val2
          nginx  nginx.swoole.local:80/index.php/test/index?field=val&field2=val2
          nginx  test.php.local:80/test/index?field=val&field2=val2
        */
        $uri = ltrim($requestUri,'/');
        if( $uri === '')
        {
            $arr = [
                APP_CONFIG['defaultAction']['module'],
                APP_CONFIG['defaultAction']['controller'],
                APP_CONFIG['defaultAction']['method']
            ];
        }
        else
        {
            $arr = explode('/',$uri);
            $L = count($arr);
            if( $L < 3 || $arr[2] === '')
            {
                $apiData = Base::creatApiData(1000,'必须指定模块、控制器和方法，缺一不可，例如：/api/user/login');
                return Base::arrayToJSON($apiData);
            }
        }
        return [
            'module' => $arr[0],
            'controller' => $arr[1],
            'method' => $arr[2]
        ];
    }

    public static function getProjectConfig(): array
    {
        return APP_CONFIG;
    }

    public  static function run(array &$data, array &$taskData): string
    {
        $module = $data['module'];
        $controller = $data['controller'];
        $method = $data['method'];
        //检测模块目录是否存在
        $dir = APP_PATH.'/'.$module;
        if( false === is_dir($dir))
        {
            $apiData = Base::creatApiData(1002,"{$module}模块不存在");
            return Base::arrayToJSON($apiData);
        }
        //检测控制器文件是否存在
        $controller = ucfirst($controller);
        $file = "{$dir}/controller/{$controller}.php";
        //say('$file',$file);
        if( false === file_exists($file))
        {
            $file = true === IS_LOCAL ? $file : '';
            $apiData = Base::creatApiData(1003,"{$controller}控制器文件{$file}不存在");
            return Base::arrayToJSON($apiData);
        }
        unset($file, $dir);
        //检测控制器是否存在
        $appDirectoryName = APP_DIRECTORY_NAME;
        $theSpecifyClass = "{$appDirectoryName}\\{$module}\\controller\\{$controller}";
        try{
            $reflectionClass = new  \ReflectionClass($theSpecifyClass);
            //检测控制器中的方法是否存在
            if( false === $reflectionClass->hasMethod($method))
            {
                $apiData = Base::creatApiData(1005,"{$module}模块的{$controller}控制器不存在{$method}()");
                return Base::arrayToJSON($apiData);
            }
            //是否为public
            $reflectionMethod = new \ReflectionMethod($theSpecifyClass,$method);
            $isPublic = $reflectionMethod->isPublic();
            if( false === $isPublic){
                $apiData = Base::creatApiData(1006,"{$module}模块的{$controller}控制器的{$method}()不是公开的方法");
                return Base::arrayToJSON($apiData);
            }
            //
            $iniMethod = 'initialize';
            if( false === $reflectionClass->hasMethod($iniMethod)){
                $isNeedInvokeInitialize = false;
            }
            else{
                $iniReflectionMethod = new \ReflectionMethod($theSpecifyClass,$iniMethod);
                $isNeedInvokeInitialize = $iniReflectionMethod->isPublic();
            }
            unset($iniReflectionMethod);
        }
        catch(\ReflectionException $e)
        {
            /* $errFile = $e->getFile();
             $errLine = $e->getLine();*/
            $errMsg = $e->getMessage();
            /* $errTrace = $e->getTrace();
             $errCode = $e->getCode();*/
            //$result = getPHPError($errCode,$errMsg,$errFile,$errLine,$errTrace);
            $apiData = Base::creatApiData(9999, $errMsg);
            return Base::arrayToJSON($apiData);
        }

        $allPHPError = [];
        //接管错误
        set_error_handler(function($errCode,$errMsg, $errFile,$errLine) use($allPHPError){
            $phpError = Debugger::getPHPError($errCode,$errMsg, $errFile,$errLine);
            $allPHPError[] = $phpError['trace'];
        });

        ob_start();
        DbBusiness::initializeInstance();
        HttpRequest::initializeInstance();
        try{
            $get = $taskData['get'];
            $post = $taskData['post'];
            $requestMethod = strtoupper($taskData['server']['request_method']);
            $isGet  = $requestMethod === 'GET';
            $isPost = $requestMethod === 'POST';
            unset($requestMethod);
            $get = $get === null ? [] : $get;
            $rawContent = &$taskData['rawContent'];
            if(true === $isPost && $post === null){
                $rawPost = json_decode($rawContent,true);
                $post = true === is_array($rawPost) ? $rawPost : [];
            }
            $post = $post === null ? [] : $post;
            //
            $instance = HttpRequest::getInstance();
            $instance->fd = $taskData['fd'];
            $instance->header = $taskData['header'];
            $instance->server = $taskData['server'];
            $instance->cookie = $taskData['cookie'];
            $instance->get = $get;
            $instance->post = $post;
            $instance->rawContent = $rawContent;
            $instance->isPost = $isPost;
            $instance->isGet  = $isGet;
            $instance->files   = $taskData['files'];
            $instance->tmpFiles = $taskData['tmpFiles'];
            $headers = &$taskData['header'];
            $instance->isAjax = self::isAjax($headers);
            $referer = self::parseReferer($headers);
            $appId = self::parseAppId($referer);
            $instance->appId = $appId;
            $origin = self::getRequestOrigin($referer, $appId);
            $instance->httpRequestOrigin = $origin;
            $instance->isFromMiniAppOrMiniGame = str_contains($origin, 'Mini');
            $remoteAddr = getItemFromArray($headers,'x-real-ip',null);
            if($remoteAddr === null){
                $remoteAddr = $taskData['server']['remote_addr'];
            }
            $instance->remoteAddr = $remoteAddr;
            $instance->sessionId = '';
            $instance->session  = [];
            $instance->module = $module;
            $instance->controller = $controller;
            $instance->method = $method;
            $projectConfig = self::getProjectConfig();
            $instance->projectConfig = $projectConfig;
            /** 初始化 session
             * 根据cookie，到session池中取出数据，赋值给SESSION，
              如果cookie中的PHPSESSID为空，需要生成唯一的sessionId
              便于业务操作*/
            $sessionKey = SessionFactory::getSessionKey();
            $requestTime = $taskData['server']['request_time'];
            $instance->requestTime = $requestTime;
            $isNeedSession = getItemFromArray($projectConfig,'isNeedSession',true);
            if(true === $isNeedSession){
                $cookiesArray = getItemFromArray($taskData, 'cookie', []);//宝塔面板，定期发起http请求，不带cookie
                $sessionId = getItemFromArray($cookiesArray, $sessionKey,null);
                if( $sessionId === null){
                    $sessionId = SessionFactory::getUniqueSessionId($taskData['fd'],$requestTime);
                }
                else{
                    $instance->session = SessionFactory::read($sessionId);
                }
                $instance->sessionId = $sessionId;
            }
            $controllerInstance = $reflectionClass->newInstance();
            if( 0 > 1){
                $controllerInstance = new Base();
            }
            $controllerInstance->httpRequest = &$instance;
            $controllerInstance->request = &$instance;
            //运行业务逻辑
            if(true === $isNeedInvokeInitialize){
                $controllerInstance->$iniMethod();
            }
            echo $reflectionMethod->invoke($controllerInstance);
            DB::stmtError();
            //
            if(true === $isNeedSession){
                if( false === empty($instance->session))
                {
                    //将SESSION弄到session池中
                    SessionFactory::write($instance->sessionId,  $instance->session);
                }
            }
        }
        catch (\Error $e)//用于捕获 Fatal error、syntax error
        {
            $errFile = $e->getFile();
            $errLine = $e->getLine();
            $errMsg = $e->getMessage();
            $errTrace = $e->getTrace();
            $errCode = $e->getCode();
            //$previous = $e->getPrevious();
            //say($previous,'$previous');
            //say('try-catch捕获错误');
            $phpError = Debugger::getPHPError($errCode,'捕获到Error：'.$errMsg, $errFile,$errLine);
            $allPHPError[] = $phpError['trace'];
        }
        catch (\Exception $e){
            $errMsg = $e->getMessage();
            $errCode = $e->getCode();
            if($errCode === 0){
                $errFile = $e->getFile();
                $errLine = $e->getLine();
                $errTrace = $e->getTrace();
               /* $previous = $e->getPrevious();
                say('$previous',$previous);*/
                //say('try-catch捕获错误',$errTrace);
                $phpError = Debugger::getPHPError($errCode,'捕获到Exception：'.$errMsg, $errFile,$errLine,$errTrace);
                $allPHPError[] = $phpError['trace'];
            }
            else{
                echo $errMsg;
            }
        }
        HttpRequest::deleteInstance();
        DbBusiness::deleteInstance();
        $content = ob_get_clean();
        $content .= self::getPhpErrorContent($allPHPError, $taskData);
        return $content;
    }

    private static function getPhpErrorContent(array &$allPHPError, array &$taskData): string
    {
        if( true === empty($allPHPError)){
            return '';
        }
        $errBody = Debugger::getAllPHPErrorAsString($allPHPError);
        if(true === APP_CONFIG['isNeedSendBugEmail']){
            ob_start();
            say('$taskData',$taskData);
            $taskContent = ob_get_clean();
            $contentToReport = "{$errBody}<hr/>任务数据：{$taskContent}";
            global $server;
            $taskId = $server->task([
                'purpose' => 'reportErrorToDevelopers',
                'content' => $contentToReport
            ]);
            if(false ===$taskId){
                Debugger::putPhpErrorContentToFile($contentToReport);
            }
        }
        return true === IS_LOCAL ? $errBody : '';
    }

    #[Pure] public static function parseReferer(array &$headers): string
    {
        return getItemFromArray($headers,'referer');
    }

    public static function parseAppId(string $referer): string
    {
        return  $referer === '' ? '' : MiniApp::parseAppId($referer);
    }

    public static function getRequestOrigin(string $referer, string $appId = ''): string
    {
        if($appId === ''){
            $appId = self::parseAppId($referer);
        }
        $isFromMiniApp = MiniApp::isFromWxMiniApp($referer);
        if(true === $isFromMiniApp){
            return self::parseMiniType($appId, 'wx');
        }
        $isFromMiniApp = MiniApp::isFromQQMiniApp($referer);
        if(true === $isFromMiniApp){
            return self::parseMiniType($appId, 'QQ');
        }
        $isFromMiniApp = MiniApp::isFromByteDanceMiniApp($referer);
        if(true === $isFromMiniApp){
            return self::parseMiniType($appId, 'byteDance');
        }
        return 'webApp';
    }

    private static function parseMiniType(string $appId, string $platform) : string {
        $type = true === str_contains(APP_CONFIG['miniAppsAndMiniGames'][$appId]['type'],'小游戏') ? 'MiniGame' : 'MiniApp';
        return $platform.$type;
    }

    #[Pure] private static function isAjax(array &$headers): bool
    {
        $fields = [
            'x-requested-with',//推荐
            'X_REQUESTED_WITH',
            'X-Requested-With',
            'http_x_requested_with',
            'HTTP_X_REQUESTED_WITH',
        ];
        foreach($fields as $field)
        {
            $v = getItemFromArray($headers,$field,null);
            if( $v !== null)
            {
                break;
            }
        }
        return  'XMLHttpRequest' === $v;
    }
}
