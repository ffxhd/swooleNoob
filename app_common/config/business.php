<?php
return (function(){
    return [
        'favicon_ico'=> ROOT.'/favicon.ico',
        'debug'=>[
            'redis' => [
                'dbIndex' => 0,
                'packName' => 'debug'
            ],
        ],
        'session'=>[
            'name'=>'PHPSESSID',
            'expireTime'=>  60 * 60 * 4,//session的过期时间(秒）,
            'redis'=>[
                'packName' => 'session'
            ],
        ],
        'defaultAction'=>[
            'module'=>'Api',
            'method'=>'index',
            'controller'=>'Index'
        ],
        //================================================
        'errorLevelExplain'=>[
            E_DEPRECATED => 'Deprecated',
            E_NOTICE => 'Notice',
            E_WARNING => 'Warning',
            E_STRICT => 'Strict',
            E_ERROR => 'Fatal Error',
            E_PARSE => '语法解析错误',
            E_CORE_ERROR => 'PHP初始化启动过程中发生致命错误',
            E_CORE_WARNING => 'PHP初始化启动过程中发生的警告 (非致命错误)',
        ],
        'jsonErrorExplain' => [
            JSON_ERROR_NONE => '没有错误发生',
            JSON_ERROR_DEPTH =>'到达了最大堆栈深度',
            JSON_ERROR_STATE_MISMATCH =>'无效或异常的 JSON',
            JSON_ERROR_CTRL_CHAR =>'控制字符错误，可能是编码不对',
            JSON_ERROR_SYNTAX =>'语法错误',
            JSON_ERROR_UTF8 =>'异常的 UTF-8 字符，也许是因为不正确的编码。',
            JSON_ERROR_RECURSION =>'One or more recursive references in the value to be encoded',
            JSON_ERROR_INF_OR_NAN =>'One or more NANor INFvalues in the value to be encoded',
            JSON_ERROR_UNSUPPORTED_TYPE =>'指定的类型，值无法编码。',
            JSON_ERROR_INVALID_PROPERTY_NAME =>'指定的属性名无法编码。',
            JSON_ERROR_UTF16 =>'畸形的 UTF-16 字符，可能因为字符编码不正确。',
        ],
    ];
})();

