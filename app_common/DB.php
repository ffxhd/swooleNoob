<?php
namespace app_common;
//工厂类,用于统一管理类的实例化
//use app_common\core\db\MySQLiMultipleConnections as SpecifyDb;
use app_common\core\db\tradition\MySQLiConnection as SpecifyDb;
use Exception;
use JetBrains\PhpStorm\Pure;
use mysqli_stmt;
use Swoole\Database\MysqliStatementProxy;

class DB{
    private static ?SpecifyDb $db = null;//注意：这个属性会跨请求

    public static function setConfig(array $config): void
    {
        self::$db = new SpecifyDb($config);
    }

    public static function closeCoroutinePool(): void
    {
        self::$db->closeCoroutinePool();
    }

    public static function closeConnection(): void
    {
        self::$db->closeConnection();
    }
    
//=====================================================================================

    /**
     * @throws Exception
     */
    public static function query(string $sql): \mysqli_result|bool
    {
        //self::selectDatabase();
		return self::$db->query($sql);
	}

    public static function getAffectedRows(): int
    {
        return self::$db->getAffectedRows();
    }

    public static function getInsertedId(): int{
        return  self::$db->getInsertedId();
    }

    /**
     * @throws Exception
     */
    public static function multiQuery(array $sqlArr): bool
    {
        //self::selectDatabase();
		return self::$db ->multiQuery($sqlArr);
	}

    /**
     * @throws Exception
     */
    public static function multiFind(array $sqlArr): array
    {
        //self::selectDatabase();
        return self::$db ->multiFind($sqlArr);
    }

    /**
     * @throws Exception
     */
    public static function findAll(string $sql): array
    {
        //self::selectDatabase();
		return self::$db->findAll($sql);
	}

    /**
     * @throws Exception
     */
    public static function findOne(string $sql): array
    {
        //self::selectDatabase();
		return self::$db->findOne($sql);
	}

    /**
     * @throws Exception
     */
    public static function findResult(string $sql, string $field, int $row=0)
	{
        //self::selectDatabase();
		$query  = self::$db->query($sql);
		return self::$db->findResult($query, $row, $field);
	}

    /**
     * @throws Exception
     */
    public static function findResultFromTheInfo(string $sql, string $field, mixed $defaultValue = null): float|int|string|null
    {
        //self::selectDatabase();
        return self::$db->findResultFromTheInfo($sql,$field, $defaultValue);
    }

    public static function startTransaction(): void
    {
        self::$db->startTransaction();
    }

    public static function commitTransaction(): void
    {
        self::$db->commitTransaction();
    }

    public static function rollBackTransaction(): void
    {
        self::$db->rollBackTransaction();
    }

    /**
     * @throws Exception
     */
    public static function insert(string $table, array $arr): bool|int
    {
        //self::selectDatabase();
		return self::$db->insert($table,$arr);
	}

    public static function buildSqlToInsert(string $table, array $arr): string
    {
        //self::selectDatabase();
        return self::$db->buildSqlToInsert($table, $arr, false);
    }

    /**
     * @throws Exception
     */
    public static function update(string $table, array $arr, array|string $where): bool|int
    {
        //self::selectDatabase();
		return self::$db->update($table, $arr, $where);
	}

    /**
     * 增加指定数据表的指定字段的计数
     * @param string $table
     * @param string $where
     * @param string $countField
     * @param int $quantity
     * @return int|bool
     * @throws Exception
     */
    public static function increaseItsCount(string $table, string $where, string $countField, int $quantity=1): int|bool
    {
        return self::changeItsCount($table, $where, $countField, $quantity);
    }

    /**
     * 减少指定数据表的指定字段的计数
     * @param string $table
     * @param string $where
     * @param string $countField
     * @param int $quantity
     * @return int|bool
     * @throws Exception
     */
    public static function decreaseItsCount(string $table, string $where, string $countField, int $quantity=1): int|bool
    {
        return self::changeItsCount($table, $where, $countField, 0 - $quantity);
    }

    /**
     * 增减指定数据表的指定字段的计数
     * @param string $table
     * @param string $where
     * @param string $countField
     * @param int $quantity
     * @return int|bool
     * @throws Exception
     */
    private static function changeItsCount(string $table, string $where, string $countField, int $quantity): int|bool
    {
        //self::selectDatabase();
        if( 0 === $quantity){
            return true;
        }
        $vToSet = $quantity > 0 ? "if(`{$countField}` is null ,{$quantity}, `{$countField}` + {$quantity})" :
            "if({$countField} = 0, 0,`{$countField}` {$quantity}) ";
        $sql = "update `{$table}` set `{$countField}` = {$vToSet} where {$where}";
        return  false === DB::query($sql) ? false : DB::getAffectedRows();
    }

    public static function buildSqlToUpdate(string $table, array $arr, array|string $where): string
    {
        return self::$db->buildSqlToUpdate($table, $arr, $where);
    }

    /**
     * 删除
     * @param $table
     * @param $where
     * @return int|bool
     * @throws Exception
     */
	public static function delete(string $table, array|string $where): int|bool
    {
        //self::selectDatabase();
		return self::$db->delete($table, $where);
	}

    /**
     * 获取预处理错误
     * @return bool|null
     * @throws Exception
     */
	public static function stmtError(): ?bool
    {
        if( self::$db === null)
        {
            return null;
        }
        return self::$db->stmtError();
    }

    public static function isStmtSucceed(mysqli_stmt|MysqliStatementProxy $stmt): bool
    {
        return self::$db->isStmtSucceed($stmt);
    }

    /**
     * @throws Exception
     */
    public static function getStmt(string $preSql): MysqliStatementProxy|null|\mysqli_stmt
    {
        //self::selectDatabase();
        return self::$db->preDeal($preSql);
    }

    public static function buildPreInsertSql(string $table, array $arr): string
    {
        return self::$db->buildSqlToInsert($table, $arr, true);
    }

    public static function buildPreUpdateSql(string $table, array $arr, array $where): string
    {
        return self::$db->sqlForUpdate($table, $arr, $where);
    }

    //========================================================================================

    #[Pure] public static function fetchSqlArr(): array
    {
        if( self::$db === null)
        {
            return [];
        }
        return self::$db->fetchSqlArr();
    }

    /**
     * @throws Exception
     */
    public static function showTables(string $db_name): array
    {
        $sql = "show tables from {$db_name}";
        $query = self::$db->query($sql);
        return self::$db->findAll($query);
    }

    /**
     * @throws Exception
     */
    public static function resetTable(string $table): void
    {
        //self::selectDatabase();
        //清空表,从1开始
        $sqlArr = [];
        $sqlArr[] = "TRUNCATE TABLE `{$table}`";
        $sqlArr[] = "ALTER TABLE `{$table}` AUTO_INCREMENT = 1";;
        self::$db->multiFind($sqlArr);
    }

    /**
     * @throws Exception
     */
    public static function resetPrimaryKey(string $table, string $primaryKey='id'): bool
    {
        $sqlArr = [
            'a' => "ALTER TABLE `{$table}` DROP `{$primaryKey}`", #删除主键
            'b' => "ALTER TABLE `{$table}` ADD `{$primaryKey}` int( 10 ) NOT NULL AUTO_INCREMENT,".
                "ADD PRIMARY KEY(`{$primaryKey}`)" #重建主键"
        ];
        //self::selectDatabase();
        return self::$db->multiQuery($sqlArr);
    }

    /**
     * @throws Exception
     */
    public static function addFields(string $table, array $data): \mysqli_result|bool
    {
        $sql = "show columns from `{$table}`";
        $struct = DB::findAll($sql);
        $existedFields = array_column($struct,'Field');
        //
        $submitFields = array_keys($data);
        $fields = array_diff($submitFields,$existedFields);
        if( true === empty($fields)){
            return true;
        }
        //self::selectDatabase();
        $sql= "alter table `{$table}`";
        $alter = [];
        foreach ($fields as $field){
            $alter[] = "add `{$field}` varchar(100)";
        }
        $alter = implode(',',$alter);
        $sql.= $alter;
        return DB::query($sql);
    }
}
