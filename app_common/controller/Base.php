<?php
namespace app_common\controller;
use app_common\DB;
use JetBrains\PhpStorm\ArrayShape;
use must\develop\Debugger;

class Base
{
    public HttpRequest $httpRequest;
    public HttpRequest $request;

    /**
     * @param string $msg
     * @param array $data
     * @param array $extraData
     * @return string
     */
    protected function success(string $msg, array $data=[], array $extraData=[]): string
    {
        $outputData = self::creatApiData(0,$msg,$data);
        $outputData = array_merge($outputData,$extraData);
        return $this->fillApiData($outputData);
    }

    /**
     * @param string $msg
     * @param array $data
     * @return string
     */
    protected function error(string $msg, array $data=[]): string
    {
        $outputData = self::creatApiData(1,$msg,$data);
        return $this->fillApiData($outputData);
    }

    /**
     * @param bool $isSuccess
     * @param array $msgArr
     * @return string
     */
    protected function successOrError(bool $isSuccess, array $msgArr): string
    {
       $errorCode = true === $isSuccess ? 0 : 1;
        $outputData = self::creatApiData($errorCode,$msgArr[$errorCode]);
        return $this->fillApiData($outputData);
    }

    #[ArrayShape([
        'errorCode' => "",
        'message' => "",
        'data' => "array|mixed"
    ])]
    public static function creatApiData(int $errorCode, string $msg, array $data = []): array
    {
        return  [
            'errorCode' => $errorCode,
            'message' => $msg,
            'data' => $data,
        ];
    }

    public static function arrayToJSON(array &$data): string
    {
        $str = json_encode($data,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        return true === is_string($str) ? $str : Debugger::jsonDecodeError();
    }

    /**
     * @param array $data
     * @return string
     */
    private function fillApiData(array $data): string
    {
        $requestInstance = HttpRequest::getInstance();
        $ip = $requestInstance->remoteAddr;
        if( true === IS_LOCAL)
        {
            $sqlArr = DB::fetchSqlArr();
            foreach ($sqlArr as &$sql){
                $sql = str_replace("\r\n",'',$sql);
            }
            $data['sqlArr'] = $sqlArr;
            $data['ip'] = $ip;
        }
        $requestContentType = $requestInstance->header['content-type'];
        $developHostIPArr = getItemFromArray(APP_CONFIG,'developer_host_ip_arr',[]);
        if( true === in_array($ip, $developHostIPArr) && $requestContentType !== 'application/json')
        {
            unset($data['original_request']);
            if( false === $requestInstance->isAjax )
            {
                $str = json_encode($data,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT );
                $jsHelp = <<<EOF
<script type="text/javascript">setTimeout(function(){window.scroll(0,0);},100);</script>
EOF;
                return '<pre style="font-size:22px;white-space:pre-wrap;">'.$str . '</pre>'.$jsHelp;
            }
        }
        return self::arrayToJSON($data);
    }

    public function fatalError(){
        liWei();
    }

    public function updateProjectsByGit(){
        echo 'hello,gitee';
    }

    public function keepAlive(){
        echo '你好呀，宝塔面板';
    }
}


