<?php
namespace app_common\controller;

use JetBrains\PhpStorm\Pure;
use Throwable;

class BusinessException extends \Exception
{
    #[Pure] public function __construct(string $message = "", int $code = -1, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}