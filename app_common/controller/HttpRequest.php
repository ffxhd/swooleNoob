<?php


namespace app_common\controller;


use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use must\CoroutineContext;

final class HttpRequest
{
    public  int    $fd;
    public  int    $requestTime = 1597238794;
    public  array  $session = [];
    public  array  $projectConfig = [];
    public  array  $header = [];
    public  array  $server = [];
    public  ?array $cookie = [];
    public  ?array $get = [];
    public  ?array $post = [];
    public  ?array $files = [];
    public  ?array $tmpFiles = [];
    public  bool   $isGet  = false;
    public  bool   $isPost = false;
    public  bool   $isAjax = false;
    public  string $rawContent = '';
    public  string $httpRequestOrigin = ''; //http请求的来源：微信小程序？微信小游戏？QQ小程序？QQ小游戏？浏览器/webApp？
    public  bool   $isFromMiniAppOrMiniGame = false;
    public  string $appId = '';//小程序的appId
    public  string $remoteAddr = '';
    public  string $sessionId = '';
    public  string $module = '';
    public  string $controller = '';
    public  string $method = '';

    /**
     * 为了单例模式
     * Request constructor.
     */
    private function __construct()
    {

    }

    private static function buildKey(): string
    {
        return 'HttpRequestInstance';
    }

    final public static function getInstance(): HttpRequest
    {
        $k = self::buildKey();
        return CoroutineContext::get($k);
    }

    final public static function initializeInstance(): void
    {
        $class = __CLASS__;
        $instance = new $class();
        $k = self::buildKey();
        CoroutineContext::put($k, $instance);
    }

    final public static function deleteInstance(): void
    {
        $k = self::buildKey();
        CoroutineContext::delete($k);
    }

    #[Pure] final public function getPost(string $field = '', $default = null){
        return $field === '' ? $this->post : getItemFromArray($this->post,$field, $default);
    }

    #[Pure] final public function getQuery(string $field = '', $default = null){
        return $field === '' ? $this->get : getItemFromArray($this->get,$field, $default);
    }

    final public function getFormatRequestTime(): string
    {
        return date('Y-m-d H:i:s', $this->getRequestTimestamp());
    }

    final public function getCurrentDate(): string
    {
        return date('Y-m-d', $this->getRequestTimestamp());
    }

    final public function getRequestTimestamp(){
        return $this->server['request_time'];
    }

    final public static function getPageSizeToSearchList(array $params,
                                                                 string $configField = 'pageSize',
                                                                 string $paramName = 'pageSize'): int
    {
        $defaultPageSize = self::getInstance()->projectConfig[$configField];
        $pageSize = getItemFromArray($params, $paramName, $defaultPageSize);
        $pageSize = intval($pageSize);
        if($pageSize <= 0){
            $pageSize = $defaultPageSize;
        }
        return $pageSize;
    }
}