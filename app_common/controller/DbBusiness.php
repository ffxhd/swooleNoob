<?php
namespace app_common\controller;

use app_common\DB;
use must\CoroutineContext;
use mysqli;
use mysqli_stmt;
use Swoole\Database\MysqliProxy;
use Swoole\Database\MysqliStatementProxy;

final class DbBusiness
{
    public bool $isInTransaction = false;
    public  mysqli|MysqliProxy|null $MySQLiConnection = null;
    public  mysqli_stmt|MysqliStatementProxy|null $stmt = null;
    public array  $sqlArr = [];

    /**
     * 为了单例模式
     * Request constructor.
     */
    private function __construct()
    {

    }

    private static function buildKey(): string
    {
        return 'DbBusinessInstance';
    }

    final public static function getInstance(): DbBusiness|null
    {
        $k = self::buildKey();
        return CoroutineContext::get($k);
    }

    final public static function initializeInstance(): void
    {
        $class = __CLASS__;
        $instance = new $class();
        $k = self::buildKey();
        CoroutineContext::put($k, $instance);
    }

    final public static function deleteInstance(): void
    {
        $sqlArr = DB::fetchSqlArr();
        if( false === empty($sqlArr)){//如果有进行增删改查
            DB::closeConnection();
        }
        $k = self::buildKey();
        CoroutineContext::delete($k);
    }
}