<?php
/*******************************************************************
 * Copyright (c) 2019 Olerp. All Rights Reserved.
 * Author: wpf <hello_ffxhd@163.com>
 * Date: 2019-9-5 15:32
 *******************************************************************/

namespace app\core;
use app\strategy\Business;
use MongoDB\Client;

class MyMongoDB extends Business
{
    const fsIdListKey = 'gridFsId';
    //
    const broken = 'broken';
    const temporary = 'temporary';
    const complete = 'complete';
    const hot = 'hot';
    //
    private  static $collectionList = [];
    //private $collectionTimeList = [];
    //
    private  static $bucketList = [];
    //private $collectionTimeList = [];

    private static function buildOption(array $fields,array $config)
    {
        $option = [];
        $option['typeMap'] = [
            'array' => 'array',
            'document' => 'array',
            'root' => 'array',
        ];
        if( false === empty($fields))
        {
            $projection = self::getProjection($fields);
            $option[ 'projection'] = $projection;
        }
        if( true === empty($config))
        {
            return $option;
        }
        $sort = getItemFromArray($config,'sort',[]);
        if( $sort !== '')
        {
            foreach ($sort as $k => &$s)
            {
                /*1 to specify ascending order.
                -1 to specify descending order.*/
                $s = $s === 'desc' ? -1 : 1;
            }
            $option[ 'sort'] = $sort;
        }
        $skip = getItemFromArray($config,'skip');
        if( $skip > 0)
        {
            $option[ 'skip'] = $skip;
        }
        $limit = getItemFromArray($config,'limit');
        if( $limit > 0)
        {
            $option[ 'limit'] = $limit;
        }
        return $option;
    }

    public  static function  getListFromCollection(string $key, array $where=[], array $fields=[],
                                              array $sort = [], int $skip = 0, int $limit = 0)
    {
        $collection = self::getCollection($key);
        $option = self::buildOption($fields,[
            'sort' => $sort,
            'skip' => $skip,
            'limit' => $limit,
        ]);
        return $collection->find($where,$option)->toArray();
    }

    public  static function getInfoFromCollection(string $key, array $where, array $fields=[])
    {
        $collection = self::getCollection($key);
        $option = self::buildOption($fields,[]);
        $document = $collection->findOne($where,$option);
        if( $document === null  )
        {
            return [];
        }
        $document['_id'] = self::getOid($document['_id']);
        $data = self::mongoDB_resultToArray($document);
        return $data;
    }

    private static function getProjection(array $fields)
    {
        $projection = [];
        foreach($fields as $key => $field)
        {
            $projection[$field] = 1;
        }
        return $projection;
    }

    public static function mongoDB_resultToArray($document)
    {
        if( true === is_array($document))
        {
            return $document;
        }
        $data = [];
        foreach($document as $key => $item)
        {
            $data[$key] = true === is_object($item) ? get_object_vars($item) : $item;
            if( true === is_array($data[$key]))
            {
                $data[$key] = self::mongoDB_resultToArray($item);
            }
        }
        return $data;
    }

    public static function getOid($obj)
    {
        return (string)$obj;
    }

    public static function insertToCollection(string $key, array $arr)
    {
        $collection = self::getCollection($key);
        $insertOneResult = $collection->insertOne($arr);
        $oidObj = $insertOneResult->getInsertedId();
        return [
            'count' => $insertOneResult->getInsertedCount(),
            'insertId' => self::getOid($oidObj),
        ];
    }

    public  static function deleteDataFromCollection(string $key, bool $onlyOne, array $where)
    {
        $collection = self::getCollection($key);
        if( true === $onlyOne)
        {
            $deleteResult = $collection->deleteOne($where);
        }
        else
        {
            $deleteResult = $collection->deleteMany($where);
        }
        return $deleteResult->getDeletedCount();
    }

    public  static function updateInfoToCollection(string $key, array $data, array $where)
    {
        $collection = self::getCollection($key);
        $updateResult =  $collection->updateOne($where, ['$set' => $data]);
        return [
            'Matched_L' => $updateResult->getMatchedCount(),
            'Modified_L' => $updateResult->getModifiedCount()
        ];
    }

    private  static function getInstance(string $host,int $port)
    {
        return  new Client("mongodb://{$host}:{$port}/");
    }

    private  static function initializeCollection(string $key)
    {
        global $config;
        $dbConfig = $config['mongoDB'][$key];
        $database = $dbConfig['database'];
        $host = $dbConfig['host'];
        $port = $dbConfig['port'];
        $collection = $dbConfig['collection'];
        return self::getInstance($host,$port)->selectDatabase($database)->selectCollection($collection);
    }

    public  static function getCollection(string $key)
    {
        if( true === isset(self::$collectionList[$key]))
        {
            return self::$collectionList[$key];
        }
        $obj = self::initializeCollection($key);
        self::$collectionList[$key] = $obj;
        return $obj;
    }

    private  static function initializeBucket(string $key)
    {
        global $config;
        $dbConfig = $config['mongoDB'][$key];
        $database = $dbConfig['database'];
        $host = $dbConfig['host'];
        $port = $dbConfig['port'];
        $gridFs = $dbConfig['gridFsBucketName'];
        return self::getInstance($host,$port)->selectDatabase($database)->selectGridFSBucket([
                'bucketName' => $gridFs,
            ]);
    }

    public static function getBucket(string $key)
    {
        if( true === isset(self::$bucketList[$key]))
        {
            return self::$bucketList[$key];
        }
        $obj = self::initializeBucket($key);
        self::$bucketList[$key] = $obj;
        return $obj;
    }

    public  static function checkKind(string $kind, $includeBroken=false)
    {
        $arr = [
             self::temporary,self::complete,self::hot
        ];
        if( true === $includeBroken)
        {
            $arr[] = self::broken;
        }
        return true === in_array($kind,$arr) ? true : implode('、',$arr);
    }

    public  static function getFileId(string $key,$stream)
    {
        $theBucket = self::getBucket($key);
        $fileId = $theBucket->getFileIdForStream($stream);
        $fileId = get_object_vars($fileId);
        return $fileId['oid'];
    }

    /*字段：chunkSize, filename, length, md5, metadata, uploadDate, _id */
    public static function getListFromBucket(string $key,array $where,array $fields,array $config = []){
        $bucket = self::getBucket($key);
        $option = self::buildOption($fields,$config);
        return $bucket->find($where,$option)->toArray();
    }

    public static function getInfoFromBucket(string $key,array $where,array $fields,array $config = [])
    {
        $bucket = self::getBucket($key);
        $option = self::buildOption($fields,$config);
        $info = $bucket->findOne($where,$option);
        if( $info === null)
        {
            return [];
        }
        return self::dealInfoFromBucket($info);
    }

    public static function dealInfoFromBucket(array &$info)
    {
        $field = '_id';
        if( true === isset($info[$field]))
        {
            $info[$field] = self::getOid($info[$field]);
        }
        return $info;
    }

}