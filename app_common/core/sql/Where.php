<?php
/**
 * Created by PhpStorm.
 * User: pf
 * Date: 2016/7/18
 * Time: 14:50
 */
namespace  app_common\core\sql;
use JetBrains\PhpStorm\Pure;

class Where{

    public static function test(): string
    {
        $keyword = $_GET['keyword'];
        $topicId = $_GET['topicId'];
        $is_public = $_GET['is_public'];
        $hisUserId = $_GET['hisUserId'];
        $whereArr = [
            'a.`content`' => ['like',$keyword],
            't.`id`'=> $topicId,
            'd.`is_public`' => $is_public,
            'u.`id`'=> $hisUserId,
        ];
        $extraWhere  = 'a.is_delete = 0';
        $where = self::combineConditions($whereArr,$extraWhere);
        //
        $orderByArr = [
            'g.`add_time`' =>getItemFromArray($_GET,'add_time',''),//desc
            'g.`price`' =>getItemFromArray($_GET,'price',''),//asc
        ];
        $orderBy = self::combineOrderBy($orderByArr,'g.add_time desc');
        return $where . ' '.$orderBy;
    }

    public  static  function combineOrderBy($orderByArr,$defaultSort)
    {
        $arr = array();
        foreach($orderByArr as $field =>$value )
        {
            $value = trim($value);
            if( $value === '')
            {
                continue;
            }
            $value = addSlashesOrNot($value);
            $arr[] = "{$field} {$value}";
        }
        if(false === empty($arr))
        {
            $order_by = implode(',',$arr);
            return 'order by '.$order_by;
        }
        else
        {
            return  'order by '.$defaultSort;
        }
    }

    /**
     * 构造搜索条件
     * @param array $whereArr
     [
        'a.`content`' => ['like',$keyword],
        'a.`status`' => ['in', $inString],
        'a.`nickname`' => ['is', 'null'],
        'a.`age`' => ['!=', 23],
        'a.`address`' => ['', 'is not null'],
        'a.`date`' => ['between', '2021-05-21', '2021-05-30'],
        't.`id`'=> $topicId,
        'd.`is_public`' => $is_public,
        'u.`id`'=> $hisUserId,
     ]
     * @param string $extraWhere
     * @return string
     */
    #[Pure] public static function combineConditions(array $whereArr, string $extraWhere = ''): string
    {
        $arr = array();
        $seq = 1;
        $seq2 = $seq + 1;
        foreach($whereArr as $field => $item )
        {
            switch(true)
            {
                case is_string($item) :
                case is_numeric($item) :
                    $value = trim($item);
                    if($value !== '')
                    {
                        $value = addSlashesOrNot($value);
                        $arr[] = self::eqValue($field, $value);
                    }
                    break;
                case is_array($item):
                    $value = getItemFromArray($item, $seq, '');
                    $value = trim($value);
                    $value = addSlashesOrNot($value);
                    $value2 = getItemFromArray($item, $seq2, '');
                    if($value2 !== ''){
                        $value2 = trim($value2);
                        $value2 = addSlashesOrNot($value2);
                    }
                    $result = self::chooseLink($field, $item[0], $value, $value2);
                    if( null !== $result)
                    {
                        $arr[] =  $result;
                    }
                    break;
                default:
            }
        }
        $condition = '';
        if(false === empty($arr))
        {
            $where = implode(' and ',$arr);
            $condition .= 'where '.$where;
        }
        if($extraWhere !== '')
        {
            $condition.= $condition === '' ? 'where '.$extraWhere  : ' and '.$extraWhere;
        }
        return $condition;
    }

    protected static function eqValue($field,$value)
    {
        switch(true)
        {
            case is_numeric($value):
                return  "{$field}={$value}";
            default:
                return "{$field}='{$value}'";
        }
    }

    protected static function chooseLink($field,$link,$value,$value2='')
    {
        switch($link)
        {
            case 'in':
                $value = $value === '' ? 'null' : $value;
                return  "{$field} in({$value})";
            case 'like':
                return $value === '' ? null : "{$field} like '%{$value}%'";
            case 'is':
                return "{$field} {$link} {$value}";
            case '>':
            case '<':
            case '>=':
            case '<=':
            case '!=':
                if($value === ''){
                    return null;
                }
                $value2 = true === is_numeric($value) ? $value : "'{$value}'";
                return "{$field} {$link} {$value2}";
            case '':
                //echo 'null';
                return "{$field}  {$value}";
            case 'between':
                $endValue = $value2;
                $startValue = $value;
                //say('$startValue',$startValue,'$endValue',$endValue);
                switch (true)
                {
                    case $startValue !== '' && $endValue !== '':
                        $startValue = true === is_numeric($startValue) ? $startValue : "'{$startValue}'";
                        $endValue   = true === is_numeric($endValue)   ?  $endValue  : "'{$endValue}'";
                        return "( {$field} between {$startValue} and  {$endValue} )";
                    case $startValue !== '' && $endValue === '':
                        $startValue = true === is_numeric($startValue) ? $startValue : "'{$startValue}'";
                        return "( {$field} >= {$startValue} ) ";
                    case $startValue === '' && $endValue !== '':
                        $endValue   = true === is_numeric($endValue)   ?  $endValue  : "'{$endValue}'";
                        return "( {$field} <= {$endValue} )";
                    default:
                        return null;
                }
            default:
                return null;
        }
    }
}
