<?php
namespace app_common\core\db\tradition;
//MySQLi面向对象
use app_common\controller\DbBusiness;
use app_common\core\db\MySQLiBusiness;
use app_common\DB;
use mysqli;

class MySQLiConnection {
    use MySQLiBusiness;
    private array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    private function putAnAvailableConnectionToPool(): void
    {

    }

    public function closeCoroutinePool(): void
    {

    }

    public function closeConnection(): void
    {
        DbBusiness::getInstance()->MySQLiConnection->close();
    }

    private function putAnEmptyConnectionToPool(): void
    {

    }

    private function getConnection(): ?mysqli
    {
        $connection = &DbBusiness::getInstance()->MySQLiConnection;
        if($connection !== null){
            return $connection;
        }
        $config = &$this->config;
        $connection = null;
        try{
            $connection = new mysqli(
                $config['host'],
                $config['user'],
                $config['password'],
                $config['database'],
                $config['port']
            );
            $connectError = $connection->connect_error;
            $connectErrNo = intval($connection->connect_errno);
            if( $connectError)
            {
                $connectError = iconv('gbk','utf-8',$connectError);
                $this->err("<mark>{$connectErrNo}：{$connectError}</mark>", 'mysql服务器连接失败：');
            }
            if(!$connection->set_charset ($config['charset']))
            {
                $this->err("<mark>set_charset 错误：错误码：{$connection->errno}；详细的报错信息：{$connection->error}</mark>");
            }
            DbBusiness::getInstance()->MySQLiConnection = &$connection;
        }
        catch (\Exception $e)
        {
            say('捕获到异常 new \mysqli',$e);
        }
        catch (\Error $e){
            say('捕获到错误 for new \mysqli',$e);
        }
        return $connection;
    }
}

