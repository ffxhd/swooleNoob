<?php
namespace app_common\core\db\coroutinePool;
//MySQLi面向对象
use app_common\controller\DbBusiness;
use app_common\core\db\MySQLiBusiness;
use mysqli;
use Swoole\Database\MysqliConfig;
use Swoole\Database\MysqliPool;
use Swoole\Database\MysqliProxy;

class MySQLiConnection {
    use MySQLiBusiness;
    private array $config;
    private MysqliPool $coroutinePool;

    public function __construct(array $config)
    {
        $this->config = $config;
        $obj = new MysqliConfig();
        $obj->withHost($config['host'])
            ->withPort($config['port'])
            // ->withUnixSocket('/tmp/mysql.sock')
            ->withDbName($config['database'])
            ->withCharset($config['charset'])
            ->withUsername($config['user'])
            ->withPassword($config['password']);
        $this->coroutinePool = new MysqliPool($obj, $config['poolSize']);
    }

    private function putAnAvailableConnectionToPool(): void
    {
        $config = &$this->config;
        DbBusiness::getInstance()->MySQLiConnection = new mysqli(
            $config['host'],
            $config['user'],
            $config['password'],
            $config['database'],
            $config['port']
        );
        $this->coroutinePool->put(DbBusiness::getInstance()->MySQLiConnection);
    }

    public function closeCoroutinePool(): void
    {
        $this->coroutinePool->close();
    }

    private function initializeConnection(): void
    {
        $connection = $this->coroutinePool->get();
        $connection->set_charset ($this->config['charset']);
        DbBusiness::getInstance()->MySQLiConnection = &$connection;
    }

    public function closeConnection(): void
    {
        $this->coroutinePool->put(DbBusiness::getInstance()->MySQLiConnection);
        DbBusiness::getInstance()->MySQLiConnection = null;
    }

    private function putAnEmptyConnectionToPool(): void
    {
        $this->coroutinePool->put(null);
        DbBusiness::getInstance()->MySQLiConnection = null;
    }

    private function getConnection(): mysqli|MysqliProxy
    {
        if(null === DbBusiness::getInstance()->MySQLiConnection){
            $this->initializeConnection();
        }
        return DbBusiness::getInstance()->MySQLiConnection;
    }
}

