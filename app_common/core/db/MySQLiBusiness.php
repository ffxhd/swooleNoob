<?php


namespace app_common\core\db;
use app_common\controller\DbBusiness;
use app_common\core\sql\Where;
use Exception;
use JetBrains\PhpStorm\Pure;
use mysqli_stmt;
use Swoole\Database\MysqliStatementProxy;

trait MySQLiBusiness
{
    private array $errNoArray = [
        'ConnectionRefused' => 2002,//No route to host
        'ConnectionResetByPeer' => 104,//Notice : send of 5 bytes failed with errno=104 Connection reset by peer
    ];

    private function addLog($str): void
    {
        $content = date('Y-m-d H:i:s').PHP_EOL.$str.PHP_EOL.PHP_EOL;
        file_put_contents(APP_PATH.'/data/debug/mysqli_log.html', $content, FILE_APPEND);
    }

    /**报错函数
     * @param string $error
     * @param string $errPrefix
     * @throws Exception
     */
    private function err(string $error, string $errPrefix = '对不起，您的操作有误，原因是:')
    {
        $str = <<<EOF
            <div style="font-size: 30px;margin-left:2%;margin-right:2%;">
                <span style="color:red;">{$errPrefix}</span>
                <div style="display:inline-block;background-color:#FFFF00;">{$error}</div>
            </div><br/>
EOF;
        $this->addLog($str);
        throw new Exception($str);
    }

    #[Pure] public function fetchSqlArr(): array
    {
        return DbBusiness::getInstance()->sqlArr;
    }

    /**
     * 执行sql
     * @param $mix
     * @return mixed
     * @throws Exception
     */
    private function queryLogic($mix): mixed
    {
        $query = null;
        $sql = '';
        try{
            $connection = $this->getConnection();
            if(false === is_object($connection))
            {
                $this->addLog('$this->getConnection()不是一个对象'.gettype($connection));
                return false;
            }
            $isMulti = is_array($mix);
            if(true === $isMulti){
                $sqlArr = &$mix;
                DbBusiness::getInstance()->sqlArr[] = $sqlArr;
                $sql = implode(';',$sqlArr);
                $query = $connection->multi_query($sql);
            }
            else{
                $sql = &$mix;
                DbBusiness::getInstance()->sqlArr[] = $sql;
                $query = $connection->query($sql);
            }
            //如果sql语句执行不成功，立即抛出异常
        }
        catch (\Throwable $throwable){
            $errNo = $throwable->getCode();
            $reason = $throwable->getMessage();
            $this->addLog("查询{$sql}, <br/>发生Throwable : 【{$errNo}】:{$reason}<br/>\$query的类型是".gettype($query));
            if( $errNo === SWOOLE_MYSQLND_CR_SERVER_GONE_ERROR ||
                $errNo === SWOOLE_MYSQLND_CR_SERVER_LOST ||
                $errNo === SWOOLE_MYSQLND_CR_CONNECTION_ERROR ||
                $errNo === $this->errNoArray['ConnectionResetByPeer'])
            {
                $this->addLog("由于【{$errNo}】:{$reason}--重新连接MySQL--");
                /*
                $this->closePool();
                $this->addLog('关闭连接池');
                //
                $this->createPool();
                $this->addLog('重建连接池, 开始递归执行 return $this->queryLogic($mix)');
                */
                if(true === DbBusiness::getInstance()->isInTransaction){
                    $this->putAnAvailableConnectionToPool();
                }
                else{
                    $this->putAnEmptyConnectionToPool();
                }
                $this->addLog('有连接对象出现异常不可重用，归还一个空连接以保证连接池的数量平衡, '.
                    '开始递归执行 return $this->queryLogic($mix)');
                //
                return $this->queryLogic($mix);
            }
            if( $errNo === $this->errNoArray['ConnectionRefused'] ) {
                $this->addLog("连接MySQL--ConnectionRefused");
                return false;
            }
            //如果没有发生以上列举的错误，则必然执行-比如连接没问题，但是sql语句有问题
            $this->err("您的SQL语句：<mark>{$sql}</mark><br/>".
                "错误：<span style=\"background-color:yellow;color:red\">{$reason}</span>");
        }
        return $query;
    }

    /**执行sql语句
     * @param string $sql
     * @return \mysqli_result|bool 返回执行成功、资源或执行失败
     * @throws Exception
     */
    public function query(string $sql): \mysqli_result|bool
    {
        return $this->queryLogic($sql);
    }

    /**执行多条sql语句
     * @param array $sqlArr
     * @return bool
     * @throws Exception
     */
    public function multiQuery(array $sqlArr): bool
    {
        return $this->queryLogic($sqlArr);
    }

    /**
     * @param array $sqlArr
     * @return array
     * @throws Exception
     */
    public function multiFind(array $sqlArr): array
    {
        $query = $this->multiQuery($sqlArr);
        //$result = $query->fetch_all();
        if( false === $query)
        {
            return [];
        }
        $connection = $this->getConnection();
        $keyArr = array_keys($sqlArr);
        $allData = [];
        $i = 0;
        do
        {
            if( $i > 0)
            {
                $connection->next_result();
            }
            $i++;
            $result = $connection->store_result();// 存储第一个结果集
            if( true === is_object($result) )
            {
                //$L = $result->num_rows;
                //say('$L',$L);
                $data = $result->fetch_all(MYSQLI_ASSOC);
                $allData[] = $data;
                $result->free();
            }
            else
            {
                $allData[] = [];
            }
        }
        while ($connection->more_results());
        return array_combine($keyArr,$allData);
    }

    /**
     * 列表
     * @param string $sql
     * @return array
     * @throws Exception
     */
    public function findAll(string $sql): array
    {
        $query = $this->query($sql);
        if( false === $query)
        {
            return [];
        }
        $result = $query->fetch_all(MYSQLI_ASSOC);
        $query->free();
        return $result;
    }

    /**
     * 单条
     * @param string $sql
     * @return array
     * @throws Exception
     */
    public function findOne(string $sql): array
    {
        $query = $this->query($sql);
        if( false === $query)
        {
            return [];
        }
        $rs = $query->fetch_array(MYSQLI_ASSOC );
        $query->free();
        return $rs? $rs : array();
    }

    /**
     * 获取指定单条记录指定字段的值
     * @param string $sql
     * @param string $field
     * @param mixed $defaultValue
     * @return string|int|float|null
     * @throws Exception
     */
    public function findResultFromTheInfo(string $sql, string $field, mixed $defaultValue): string|int|float|null
    {
        $query = $this->query($sql);
        if( false === $query)
        {
            return $defaultValue;
        }
        $obj = $query->fetch_object();
        $query->free();
        return null === $obj ? $defaultValue : $obj->$field;
    }

    /**
     * 指定行的指定字段的值
     * @param \mysqli_result $query $query sql语句通过$conn->query执行出的来的资源
     * @param integer $row
     * @param string $field 指定字段的
     * @return mixed 返回指定行的指定字段的值
     */
    public function findResult(\mysqli_result $query, int $row, string $field): mixed
    {
        if( false === $query)
        {
            return [];
        }
        /* fetch object array */
        $i = 0;
        while ( $obj  =  $query ->fetch_object())
        {
            if($i == $row )
            {
                return $obj -> $field;
            }
            $i ++;
        }
        $query->free();
        return '';
    }

    /**添加函数
     * @param string $table 表名
     * @param array $arr 添加数组（包含字段和值的一维数组）
     * @return bool|int
     * @throws Exception
     */
    public function insert(string $table, array $arr): bool|int
    {
        $sql = $this->buildSqlToInsert($table,$arr,false);
        $query = $this->query($sql);
        return false === $query ? false : $this->getInsertedId();
    }

    public function buildSqlToInsert(string $table, array $arr, bool $isPreInert): string
    {
        $keyArr = array();
        $valueArr = array();
        foreach($arr as $field => &$value)
        {
            $value = $this->washValue($value);
            $keyArr[] = "`{$field}`";
            if($isPreInert=== true){
                $valueArr[] = '?';
            }
            else{
                $valueArr[] = $value === null ? null :  "'{$value}'";
            }
        }
        $keys   = implode(',',$keyArr);
        $values = implode(',',$valueArr);
        return  "insert into `{$table}` ({$keys}) values ({$values});";
    }

    public function sqlForUpdate(string $table, array $dataList, array $whereList): string
    {
        $toSet = array();
        foreach ($dataList as $arr)
        {
            foreach($arr as $field => $fieldVal )
            {
                $toSet[] = "`{$field}` = ? ";
            }
            break;
        }
        $toSet = implode(',',$toSet);
        //
        $where = [];
        foreach ($whereList as $whereArr)
        {
            foreach( $whereArr as $field => $fieldVal )
            {
                $where[] = "`{$field}` = ? ";
            }
            break;
        }
        $where = implode(' and ',$where);
        //
        return  "update `{$table}`  set  {$toSet}  where  {$where}";
    }

    /**
     * 预处理
     * @param string $preSql
     * @return bool|mysqli_stmt|MysqliStatementProxy
     * @throws Exception
     */
    public function preDeal(string $preSql): null|mysqli_stmt|MysqliStatementProxy
    {
        $connection = $this->getConnection();
        //例如$preSql="INSERT INTO MyGuests (firstname, lastname, email) VALUES(?, ?, ?)";
        $stmt = $connection->prepare($preSql);//插入，更新
        if(false === $stmt)
        {
            //say('error',$conn->error,'error_list',$conn->error_list,'get_warnings',$conn->get_warnings());
            $this->err($connection->error,"预处理语句有错误：{$preSql}");
        }
        DbBusiness::getInstance()->stmt = &$stmt;
        return $stmt;
        //注意 主键未设置自增，循环插入会全部失败--2017-12-22
        /* 以下外界使用
         * $stmt->bind_param("sss", $firstname, $lastname, $email);
        // 设置参数并执行
        $firstname = "John";
        $lastname = "Doe";
        $email = "john@example.com";
        $stmt->execute();

        $firstname = "Mary";
        $lastname = "Moe";
        $email = "mary@example.com";
        $stmt->execute();

        $firstname = "Julie";
        $lastname = "Dooley";
        $email = "julie@example.com";
        $stmt->execute();
        $stmt->close();
        $conn->close(); */
    }

    public function isStmtSucceed(mysqli_stmt|MysqliStatementProxy $stmt): bool
    {
        return $stmt->errno === 0;
    }

    /**
     * 预处理语句执行出错
     * @param string $errPrefix
     * @return bool
     * @throws Exception
     */
    public function stmtError(string $errPrefix = '预处理语句执行出错'): bool
    {
        //$stmt->affected_rows === -1
        //say('$this->stmt',$this->stmt);
        $stmt = &DbBusiness::getInstance()->stmt;
        if( false === is_object($stmt))
        {
            return false;
        }
        $errorList = $stmt->error_list;
        $error = $stmt->error;
        if( false === empty($errorList))
        {
            $a = print_r($errorList,true );
            $a = "<pre>{$a}</pre>";
            $this->err($a, $errPrefix);
        }
        else if($error)
        {
            $this->err($error, $errPrefix);
        }
        return true;
    }

    /**修改函数
     * @param string $table 表名
     * @param array $arr 修改数组（包含字段和值的一维数组）
     * @param string|array $where 条件
     * @return bool|int
     * @throws Exception
     */
    public function update(string $table, array $arr, array|string $where): bool|int
    {
        $sql = $this->buildSqlToUpdate($table,$arr,$where);
        $query = $this->query($sql);
        return $query === false ? false : $this->getAffectedRows();
    }

    public function buildSqlToUpdate(string $table, array $arr, array|string $where): string
    {
        $keyWithValArr = array();
        foreach($arr as $key => &$value)
        {
            $value = $this->washValue($value);
            $keyWithValArr[]= $value === null ? "{$key} = null " : "`{$key}` = \"{$value}\"";//'`'.$key."`='".$value."'";
        }
        $keyAndValues=implode(',',$keyWithValArr);
        $where = $this->washWhere($where);
        return "update `{$table}` set {$keyAndValues} {$where};";
    }

    private function washValue(null|int|string $value): null|int|string
    {
        if(null === $value){
            return null;
        }
        if(true === is_integer($value)){
            return $value;
        }
        //会被进行转义的字符包括： NUL （ASCII 0），\n，\r，\，'，" 和 Control-Z.
        return $this->getConnection()->real_escape_string($value);
    }

    /**
     * 清洗where条件
     * @param array|string $where
     * @return string
     */
    private function washWhere(array|string $where): string
    {
        if(true === is_array($where))
        {
            foreach($where as $key => &$value) {
                $value = $this->washValue($value);
            }
            return where::combineConditions($where);
        }
        if( true === is_string($where)  )
        {
            return stripos($where,'where') === false ? ' where '.$where : $where;
        }
        return '';
    }

    /**删除函数
     * @param string $table
     * @param string|array $where
     * @return bool|int
     * @throws Exception
     */
    public function delete(string $table, array|string $where): bool|int
    {
        $where = $this->washWhere($where);
        $sql = "delete from  `{$table}` {$where}";
        $query = $this->query($sql);
        return $query === false ? false : $this->getAffectedRows();
    }

    public function startTransaction(): void
    {
        $connection = $this->getConnection();
        DbBusiness::getInstance()->isInTransaction = true;
        $connection->query('SET AUTOCOMMIT=0');//设置为不自动提交，因为MYSQL默认立即执行
        $connection->query('BEGIN');//开始事务定义
    }

    public function commitTransaction(): void
    {
        $this->getConnection()->query('COMMIT');//执行事务
        $this->setAutoCommit();
        DbBusiness::getInstance()->isInTransaction = false;
    }

    public function rollBackTransaction(): void
    {
        $this->getConnection()->query('ROLLBACK');//判断当执行失败时回滚;
        $this->setAutoCommit();
        DbBusiness::getInstance()->isInTransaction = false;
    }

    private function setAutoCommit(): void
    {
        $this->getConnection()->query('SET AUTOCOMMIT=1');
    }

    /**
     * 获取受到影响的记录数量
     * @return int
     */
    public function getAffectedRows(): int
    {
        return $this->getConnection()->affected_rows;
    }

    /**
     * 获取插入的记录的id
     * @return int
     */
    public function getInsertedId(): int{
        return $this->getConnection()->insert_id;
    }
}