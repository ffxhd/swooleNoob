<?php
namespace app_common\core\wx;

trait WxCommon
{
    /**
     * 请求微信api之后的结果
     * @param $result
     * @param string $purpose
     * @param array $errorCodesMeans
     * @return array
     */
    final protected function buildResult($result, string $purpose,array $errorCodesMeans=[]){
        if(false === is_string($result))
        {
            return [
                'errorCode' => null,
                'message' => "{$purpose},微信没有返回数据包",
            ];
        }
        $arr = json_decode($result,true);
        if(false === is_array($arr)){
            return [
                'errorCode' => null,
                'message' => "{$purpose},微信返回的JSON数据包，解析后的数组为空",
            ];
        }
        $errorCode = getItemFromArray($arr,'errcode',0);
        $message = getItemFromArray($errorCodesMeans, $errorCode, '不在小程序订阅消息接口返回码说明内');
        $originalMessage = getItemFromArray($arr,'errmsg');
        if('' !== $originalMessage){
            $message .= "原文:{$originalMessage}";
        }
        return [
            'errorCode' => intval($errorCode),
            'message' => $message,
            'data' => $arr,
        ];
    }

    /**
     * 处理微信http返回结果
     * @param $result
     * @param $debug_msg_pre
     * @return array|mixed
     * @throws \Exception
     */
    final protected function handleHttpResult($result,$debug_msg_pre){
        if($result === false){
            throw new \Exception("请求{$debug_msg_pre}，没有获得响应数据");
        }
        $json = json_decode($result,true);
        if ( false === is_array($json))
        {
            throw new \Exception($debug_msg_pre.'微信返回的JSON数据包，不是数组');
        }
        $errCode = isset($json['errcode']) ? $json['errcode'] : '';
        if ( $errCode > 0)
        {
            throw new \Exception("{$debug_msg_pre}微信返回的JSON数据包，包含错误信息：
                {$errCode}--{$json['errmsg']}");
        }
        return $json;
    }

    /**
     * GET 请求
     * @param $url
     * @return bool|mixed
     */
    final protected function http_get($url)
    {
        $oCurl = curl_init();
        if(stripos($url,'https://')!== false)
        {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        return intval($aStatus['http_code']) === 200 ? $sContent : false;
    }

    /**
     * POST 请求
     * @param string $url
     * @param array|string $param
     * @param boolean $post_file 是否文件上传
     * @return string content
     */
    final protected function http_post($url,$param,$post_file=false)
    {
        $searchBugMsgArr = [];
        $oCurl = curl_init();
        if(stripos($url,'https://') !== false)
        {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }
        if (PHP_VERSION_ID >= 50500 && class_exists('\CURLFile'))
        {
            $is_curlFile = true;
            $searchBugMsgArr[] = '$is_curlFile = true;';
        }
        else
        {
            $is_curlFile = false;
            $searchBugMsgArr[] = '$is_curlFile = false';
            if (defined('CURLOPT_SAFE_UPLOAD'))
            {
                $searchBugMsgArr[] = 'defined(\'CURLOPT_SAFE_UPLOAD\')';
                curl_setopt($oCurl, CURLOPT_SAFE_UPLOAD, false);
            }
        }
        if (true === is_string($param))
        {
            $strPOST = $param;
        }
        elseif($post_file)
        {
            if($is_curlFile)
            {
                foreach ($param as $key => $val)
                {
                    if (substr($val, 0, 1) == '@')
                    {
                        $param[$key] = new \CURLFile(realpath(substr($val,1)));
                    }
                }
            }
            $strPOST = $param;
        }
        else
        {
            $aPOST = array();
            foreach($param as $key=>$val)
            {
                $aPOST[] = $key.'='.urlencode($val);
            }
            $strPOST =  join('&', $aPOST);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($oCurl, CURLOPT_POST,true);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST);
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        return intval($aStatus['http_code']) === 200 ? $sContent : false;
    }

    /**
     * 微信api不支持中文转义的json结构
     * @param array  $arr
     * @return string
     */
    final protected static function json_encode($arr) {
        if (count($arr) == 0) return '[]';
        $parts = array ();
        $is_list = false;
        //Find out if the given array is a numerical array
        $keys = array_keys ( $arr );
        $max_length = count ( $arr ) - 1;
        if (($keys [0] === 0) && ($keys [$max_length] === $max_length )) { //See if the first key is 0 and last key is length - 1
            $is_list = true;
            for($i = 0; $i < count ( $keys ); $i ++) { //See if each key correspondes to its position
                if ($i != $keys [$i]) { //A key fails at position check.
                    $is_list = false; //It is an associative array.
                    break;
                }
            }
        }
        foreach ( $arr as $key => $value ) {
            if (is_array ( $value )) { //Custom handling for arrays
                if ($is_list)
                    $parts [] = self::json_encode ( $value ); /* :RECURSION: */
                else
                    $parts [] = '"' . $key . '":' . self::json_encode ( $value ); /* :RECURSION: */
            } else {
                $str = '';
                if (! $is_list)
                    $str = '"' . $key . '":';
                //Custom handling for multiple data types
                if (!is_string ( $value ) && is_numeric ( $value ) && $value<2000000000)
                    $str .= $value; //Numbers
                elseif ($value === false)
                    $str .= 'false'; //The booleans
                elseif ($value === true)
                    $str .= 'true';
                else
                    $str .= '"' . addslashes ( $value ) . '"'; //All other things
                // :TODO: Is there any more datatype we should be in the lookout for? (Object?)
                $parts [] = $str;
            }
        }
        $json = implode ( ',', $parts );
        if ($is_list)
            return '[' . $json . ']'; //Return numerical JSON
        return '{' . $json . '}'; //Return associative JSON
    }
}