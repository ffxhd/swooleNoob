<?php


namespace app_common\core\wx\publicAccount;


class WxJs
{
    //---------JS SDK--------------------------------------------------------------------------------------

    /**
     * 获取jsapi_ticket--公众号用于调用微信JS接口的临时票据
     * @param string $js_api_ticket 手动指定jsapi_ticket，非必要情况不建议用
     * @return  string
     */
    public function getJsApiTicket(  $js_api_ticket = '')
    {
        /*正常情况下，jsapi_ticket的有效期为7200秒，通过access_token来获取。
        由于获取jsapi_ticket的api调用次数非常有限，频繁刷新jsapi_ticket会导致api调用受限，
        影响自身业务，开发者必须在自己的服务全局缓存jsapi_ticket 。
        成功返回如下JSON：
        {
        "errcode":0,
        "errmsg":"ok",
        "ticket":"bxLdikRXVbTPdHSM05e5u5sUoXNKd8-41ZO3MhKoyN5OfkWITD
                  Ggnr2fwJ0m9E8NYzWKVZvdVtaUgWvsdshFKA",
        "expires_in":7200
        }
        */
        $debug_msg_pre = '获取调用微信JS接口的临时票据jsapi_ticket，';
        $noIdealData = '';
        if (!$this->access_token && !$this->getGlobalAccessToken())
        {
            $this->debug_msg = $debug_msg_pre.'没有access_token';
            return $noIdealData;
        }
        if ($js_api_ticket) //手动指定token，优先使用
        {
            $this->js_api_ticket = $js_api_ticket;
            return $this->js_api_ticket;
        }
        $data = $this->getCache( $this->js_api_ticket_cache_file,'ticket','js_api_ticket缓存');
        if( $data )
        {
            $this->js_api_ticket = $data;
            return $data;
        }
        $result = $this->http_get(self::API_URL_PREFIX.self::GET_TICKET_URL.
            'access_token='.$this->access_token.'&type=jsapi');
        if ($result)
        {
            $json = json_decode($result,true);
            if(  $this->noIdealDataInJSON($json,$debug_msg_pre) === false  )
            {
                return $noIdealData;
            }
            $this->js_api_ticket = $json['ticket'];
            $this->setCache($json, $this->js_api_ticket_cache_file,'js_api_ticket');
            return $this->js_api_ticket;
        }
        else
        {
            return $this->noResultAfterHttpRequest( $debug_msg_pre ,$noIdealData );
        }
    }

    /**
     * 生成JS-SDK权限验证的配置
     * @return array
     */
    public function get_JS_SDK_config($url='', $timestamp = '',$nonceStr ='')
    {
        $jsApiTicket = $this->getJsApiTicket();
        if(!$jsApiTicket )
        {
            return array();
        }
        // 注意 URL 一定要动态获取，不能 hardcode.
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
        $url = $url ? $url : "{$protocol}{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $timestamp = $timestamp ? $timestamp : time();
        $nonceStr = $nonceStr ? $nonceStr : $this->createNonceStr();
        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = "jsapi_ticket={$jsApiTicket}&noncestr={$nonceStr}&timestamp={$timestamp}&url={$url}";
        $signature = sha1($string);
        $signPackage = array(
            'appId'     => $this->appId, // 必填，公众号的唯一标识
            'nonceStr'  => $nonceStr,// 必填，生成签名的随机串
            'timestamp' => $timestamp,// 必填，生成签名的时间戳
            'url'       => $url,
            'signature' => $signature,//必填，JS-SDK使用权限签名
            'rawString' => $string
        );
        return $signPackage;
    }

    private function createNonceStr($length = 16)
    {
        //return 'noncestr';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        $pfSr_L = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++)
        {
            $num  = mt_rand(0, $pfSr_L);
            //$str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
            $str .= substr($chars,$num, 1);
        }
        return $str;
    }
}