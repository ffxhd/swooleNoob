<?php
namespace app_common\core\wx\publicAccount;

class WxPublicAccount
{
    use \app_common\core\wx\WxCommon;
    private string $token;//【类型：字符串】
    private string $appId;//【类型：字符串】
    private string $appSecret;//【类型：字符串】
    private string $accessTokenCacheFile;//绝对路径
    public  string $debug_msg = '';

    /**
     * WxPublicAccount constructor.
     * @param array $options
     * @throws \Exception
     */
    public function __construct(array $options)
    {
        $this->token = isset($options['token']) ? $options['token'] : '';
        $this->appId = isset($options['appId']) ? $options['appId'] : '';
        $this->appSecret = isset( $options['appSecret']) ? $options['appSecret'] : '';
        $file = isset($options['accessTokenCacheFile']) ? $options['accessTokenCacheFile'] : '';
        if($file === ''){
            throw new \Exception('accessTokenCacheFile 不能为空');
        }
        if( false === file_exists($file)){
            throw new \Exception("accessTokenCacheFile 必须存在。注意：{$file}不存在");
        }
        $this->accessTokenCacheFile = $file;
    }

    //-----------------------------------------------------------------------------------------------

    /**
     * 验证消息的确来自微信服务器
     * @param $signature
     * @param $timestamp
     * @param $nonce
     * @return bool
     */
    public function checkSignature($signature,$timestamp,$nonce)
    {
        $token = $this->token;
        //1）将token、timestamp、nonce三个参数进行字典序排序
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        //2）将三个参数字符串拼接成一个字符串进行sha1加密
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );
        //3）开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
        return $tmpStr == $signature;
    }

    //-----------------------------------------------------------------------------------------------

    /**
     * 保存access_token【公众号的全局唯一票据】
     * @param $array
     * @param $file
     * @param string $mean
     * @return bool
     */
    public function setCache($array,$file, $mean='')
    {
        if( !file_exists($file) )
        {
            $this->debug_msg = $file.'不存在--'.$mean;
            return false;
        }
        $array['expires_in'] += $_SERVER['REQUEST_TIME'];
        $array['expire_format'] = date('Y-m-d H:i:s',$array['expires_in']);
        $json = json_encode($array,JSON_UNESCAPED_UNICODE );
        $flag = file_put_contents( $file, $json );
        if( $flag === false )
        {
            $this->debug_msg = "将{$mean}写入{$file}失败";
            return false;
        }
        else
        {
           return true;
        }
    }

    /**
     * 取得保存的access_token【公众号的全局唯一票据】
     * @param string $file
     * @param string $field
     * @param string $mean
     * @return string|array
     */
    public function getCache(string $file,string $field = '',string $mean='')
    {
        $noValidData = $field ? '' : array() ;
        if( !file_exists($file) )
        {
            $this->debug_msg = $file.'不存在--'.$mean;
            return $noValidData;
        }
        $str = file_get_contents( $file );
        if( !$str )
        {
            $this->debug_msg = $file.'--无内容--'.$mean;
            return $noValidData;
        }
        $array = json_decode($str,true);
        if( !$array )
        {
            $this->debug_msg = $file.'--内容转为数组，为空--'.$mean;
            return $noValidData;
        }
        if( $array['expires_in'] < time() )
        {
            return $noValidData;
        }
        else
        {
            return $field!== '' ? $array[$field] : $array ;
        }
    }

    /**
     * 获取access_token【公众号的全局唯一票据】
     * @param string $appId 如在类初始化时已提供，则可为空
     * @param string $appSecret 如在类初始化时已提供，则可为空
     * @param string $globalAccessToken 手动指定access_token，非必要情况不建议用
     * @return string
     * @throws \Exception
     */
    public function getGlobalAccessToken(string $appId='', string $appSecret='', string $globalAccessToken='')
    {
        /*公众号的全局唯一票据，公众号调用各接口时都需使用access_token。
        开发者需要进行妥善保存。access_token的存储至少要保留512个字符空间。
        access_token的有效期目前为2个小时，需定时刷新

        正常情况下，微信会返回下述JSON数据包给公众号：
        {"access_token":"ACCESS_TOKEN","expires_in":7200}
        错误时微信会返回错误码等信息，JSON数据包示例如下（该示例为AppID无效错误）:
        {"errcode":40013,"errmsg":"invalid appid"}
        */
        $debug_msg_pre = '公众号的全局唯一票据access_token，';
        if ( $appId === '' || $appSecret === '')
        {
            $appId = $this->appId;
            $appSecret = $this->appSecret;
        }
        if ( $globalAccessToken !== '' )//手动指定token，优先使用
        {
            return $globalAccessToken;
        }
        $globalAccessToken = $this->getCache($this->accessTokenCacheFile,'access_token','access_token缓存');
        if( $globalAccessToken !== '' )
        {
            return $globalAccessToken;
        }
        //https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
        $result = $this->http_get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appId}&secret={$appSecret}");
        $json = $this->handleHttpResult($result,$debug_msg_pre);
        $this->setCache($json,$this->accessTokenCacheFile,'获取公众号的全局唯一票据access_token');
        return $json['access_token'];
    }

    //-----------------------------------------------------------------------------------------------

    /**
     * 发送客服消息
     * @param array $data 消息结构{"touser":"OPENID","msgtype":"news","news":{...}}
     * @return array
     * @throws \Exception
     */
    public function sendCustomMessage(array $data)
    {
        $debug_msg_pre = '发送客服消息，';
        $globalAccessToken = $this->getGlobalAccessToken();
        if ( $globalAccessToken === '' || $globalAccessToken === null )
        {
            throw new \Exception($debug_msg_pre.'没有access_token');
        }
        $url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$globalAccessToken}";
        $param = self::json_encode($data);
        $result = $this->http_post( $url,$param);
        return $this->handleHttpResult($result,$debug_msg_pre);
    }

    public static function buildCustomTextMessage(string $openId,string $content){
        //<a href="http://www.qq.com" data-miniprogram-appid="appid" data-miniprogram-path="pages/index/index">点击跳小程序</a>
        return [
            'touser' => $openId,
            'msgtype' => 'text',
            'text' => [
                "content" => $content
            ]
        ];
    }

    public static function buildHref(string $href, string $text, string $miniAppId, string $miniAppPage){
        /*说明： 1.data-miniprogram-appid 项，填写小程序appid，则表示该链接跳小程序；
        2.data-miniprogram-path项，填写小程序路径，路径与app.json中保持一致，可带参数；
        3.对于不支持data-miniprogram-appid 项的客户端版本，如果有herf项，则仍然保持跳href中的网页链接；
        4.data-miniprogram-appid对应的小程序必须与公众号有绑定关系。*/
        return <<<EOF
<a href="{$href}" data-miniprogram-appid="{$miniAppId}" data-miniprogram-path="{$miniAppPage}">{$text}</a>
EOF;

    }

    //-----------------------------------------------------------------------------------------------

    /**
     * 发送模板消息
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function sendTemplateMessage($data)
    {
        /*POST数据示例如下：
      {
           "touser":"OPENID",
           "template_id":"ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY",
           "url":"http://weixin.qq.com/download",
           "miniprogram":{
             "appid":"xiaochengxuappid12345",
             "pagepath":"index?foo=bar"
           },
           "data":{
                   "first": {
                       "value":"恭喜你购买成功！",
                       "color":"#173177"
                   },
                   "keynote1":{
                       "value":"巧克力",
                       "color":"#173177"
                   },
                   "keynote2": {
                       "value":"39.8元",
                       "color":"#173177"
                   },
                   "keynote3": {
                       "value":"2014年9月22日",
                       "color":"#173177"
                   },
                   "remark":{
                       "value":"欢迎再次购买！",
                       "color":"#173177"
                   }
           }
       }*/
        $debug_msg_pre = '发送模板消息，';
        $globalAccessToken = $this->getGlobalAccessToken();
        if ( $globalAccessToken === '' || $globalAccessToken === null )
        {
            throw new \Exception($debug_msg_pre.'没有access_token');
        }
        //https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN
        $result = $this->http_post("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$globalAccessToken}",self::json_encode($data));
        return $this->handleHttpResult($result,$debug_msg_pre);
    }

    //-----------------------------------------------------------------------------------------------

    /**
     * 构造用于被动回复的文本
     * @param array $receivedData
     * @param string $content
     * @return string
     */
    public static function buildTextMessage(array $receivedData, string $content){
        /*收到参数	描述
ToUserName	开发者微信号
FromUserName	发送方帐号（一个OpenID）
CreateTime	消息创建时间 （整型）
MsgType	消息类型，文本为text
Content	文本消息内容
MsgId	消息id，64位整型*/

        /*发送 参数	是否必须	描述
ToUserName	是	接收方帐号（收到的OpenID）
FromUserName	是	开发者微信号
CreateTime	是	消息创建时间 （整型）
MsgType	是	消息类型，文本为text
Content	是	回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
*/
        $receiverOpenId = $receivedData['FromUserName'];
        $fromWxNo = $receivedData['ToUserName'];
        $createTime = $receivedData['CreateTime'];
        return <<<EOF
<xml>
  <ToUserName><![CDATA[{$receiverOpenId}]]></ToUserName>
  <FromUserName><![CDATA[{$fromWxNo}]]></FromUserName>
  <CreateTime>{$createTime}</CreateTime>
  <MsgType><![CDATA[text]]></MsgType>
  <Content><![CDATA[{$content}]]></Content>
</xml>
EOF;

    }

    //-----------------------------------------------------------------------------------------------

    /**
     * 生成带参数的二维码
     * @param $path
     * @return bool|mixed
     * @throws \Exception
     */
    public function requestQrCodeWithParams($path){
        $qrCodeDataFile = APP_PATH.'/data/qrCode.txt';
        $qrCodeFile = $path.'/qrCode.jpg';
        $debug_msg_pre = '获取带参数的二维码，';
        //
        $jsonString = file_get_contents($qrCodeDataFile);
        if($jsonString !== ''){
            $arr = json_decode($jsonString,true);
            say('二维码文件是否有效 $arr',$arr);
            $currentTime = time();
            if($currentTime < $arr['expire']){
                say('直接返回有效的二维码文件');
                return $qrCodeFile;
            }
        }
        //
        $globalAccessToken = $this->getGlobalAccessToken();
        if ( $globalAccessToken === '' || $globalAccessToken === null )
        {
            throw new \Exception($debug_msg_pre.'没有access_token');
        }
        /*{ "expire_seconds": 604800,
            "action_name": "QR_STR_SCENE",
            "action_info": {
                "scene": {"scene_str": "test"}
            }
        }*/
        $data = [
            'expire_seconds' => 604800,
            'action_name' => 'QR_STR_SCENE',
            'action_info' => [
                'scene' =>[
                    'scene_str' => 'test'
                ]
            ]
        ];
        $data = self::json_encode($data);
        $result = $this->http_post("https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={$globalAccessToken}",$data);
        $json = $this->handleHttpResult($result,$debug_msg_pre);
        //通过ticket换取二维码
        /*正确的Json返回结果:
        {"ticket":"gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm3sUw==",
        "expire_seconds":60,
        "url":"http://weixin.qq.com/q/kZgfwMTm72WWPkovabbI"}
        参数	说明
        ticket	获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
        expire_seconds	该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）。
        url	二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片*/
        say('生成带参数的二维码 $json',$json);
        //
        $dataToSave = $json;
        $dataToSave['expire'] = time() + $json['expire_seconds'];
        $jsonString = json_encode($dataToSave,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        file_put_contents($qrCodeDataFile,$jsonString);
        say('保存带参数的二维码为图片的数据',$dataToSave);
        //
        $ticket = $json['ticket'];
        $ticket = urlencode($ticket);
        $result = $this->http_get("https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={$ticket}");
        say('生成带参数的二维码 showqrcode $result',$result);
        if($result !== false){
            file_put_contents($qrCodeFile,$result);
            say('保存带参数的二维码为图片');
        }
        return $qrCodeFile;
    }
}
