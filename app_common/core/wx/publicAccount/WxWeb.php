<?php


namespace app_common\core\wx\publicAccount;


class WxWeb
{
    private string  $js_api_ticket;
    private string  $js_api_ticket_cache_file;//绝对路径
    private string $user_token;//微信网页授权凭证【类型：字符串】
    private string $user_token_cache_file;//绝对路径

    public function __construct(array $options)
    {
        $this->js_api_ticket_cache_file = isset( $options['js_api_ticket_cache_file'] ) ?
            $options['js_api_ticket_cache_file'] : '';
        $this->user_token_cache_file = isset( $options['user_token_cache_file'] ) ?
            $options['user_token_cache_file'] : '';
    }

    //--------微信网页授权---------------------------------------------------------------------------------------

    /**
     * 微信网页授权  第一步：引导用户进入授权页面,建议单独一个public function
     * @param string  $redirect_uri 如果用户同意授权，页面将跳转至 redirect_uri/?code=CODE&state=STATE。
     * @param string $state
     * @param string $scope
     */
    public  function getOauthRedirect($redirect_uri,$state='',$scope='snsapi_userinfo')
    {
        $authorize_url =  self::OAUTH_PREFIX.self::OAUTH_AUTHORIZE_URL.'appid='.$this->appId.
            '&redirect_uri='.urlencode($redirect_uri).'&response_type=code&scope='.$scope.'&state='.$state.
            '#wechat_redirect';
        header( 'Location: '. $authorize_url);
    }

    /**
     * 微信网页授权  第二步：用户同意授权，获取code,通过code换取网页授权access_token（与基础支持中的access_token不同）
     * @return array
     */
    public function getOauthAccessToken()
    {
        /*正确时返回的JSON数据包如下：
            { "access_token":"ACCESS_TOKEN",
            "expires_in":7200,
            "refresh_token":"REFRESH_TOKEN",
            "openid":"OPENID",
            "scope":"SCOPE" }
         错误时微信会返回JSON数据包如下（示例为Code无效错误）:
            {"errcode":40029,"errmsg":"invalid code"}
        */
        $debug_msg_pre = '微信网页授权  通过code换取网页授权access_token，';
        $noIdealData = array();
        $data = $this->getCache( $this->user_token_cache_file,'','网页授权access_token缓存');
        if( $data )
        {
            $this->user_token = $data['access_token'];
            return $data;
        }
        $code = isset($_GET['code'])? $_GET['code']:'';
        if (!$code)
        {
            $this->debug_msg = $debug_msg_pre.'!$_GET[\'code\']';
            return $noIdealData;
        }
        $result = $this->http_get(self::API_BASE_URL_PREFIX.self::OAUTH_TOKEN_URL.
            'appid='.$this->appId.'&secret='.$this->appSecret.'&code='.$code.
            '&grant_type=authorization_code');
        if ($result)
        {
            $json = json_decode($result,true);
            if(  $this->noIdealDataInJSON($json,$debug_msg_pre) === false  )
            {
                return $noIdealData;
            }
            $this->setCache($json, $this->user_token_cache_file,'网页授权access_token');
            $this->user_token = $json['access_token'];
            return $json;
        }
        else
        {
            return $this->noResultAfterHttpRequest( $debug_msg_pre ,$noIdealData );
        }
    }

    /**
     * 检验授权凭证【类型：字符串】（access_token）是否有效
     * 微信网页授权----插曲
     * @param string $access_token 【类型：字符串】
     * @param string $openid
     * @return boolean 是否有效
     */
    public function getOauthAuth($access_token,$openid)
    {
        /*正确的JSON返回结果：
        { "errcode":0,"errmsg":"ok"}
        错误时的JSON返回示例：
        { "errcode":40003,"errmsg":"invalid openid"}
        */
        $debug_msg_pre = '检验授权凭证access_token是否有效，';
        $noIdealData = false;
        $result = $this->http_get(self::API_BASE_URL_PREFIX.self::OAUTH_AUTH_URL.
            'access_token='.$access_token.'&openid='.$openid);
        if ($result)
        {
            $json = json_decode($result,true);
            return $this->noIdealDataInJSON($json,$debug_msg_pre);
        }
        else
        {
            return $this->noResultAfterHttpRequest( $debug_msg_pre ,$noIdealData );
        }
    }

    /**
     * 微信网页授权  第三步：刷新授权凭证【类型：字符串】access token并续期
     * （如果需要，即授权凭证无效）
     * @param string $refresh_token
     * @return array
     */
    public function getOauthRefreshToken($refresh_token)
    {
        /*正确时返回的JSON数据包如下：
        { "access_token":"ACCESS_TOKEN",
         "expires_in":7200,
         "refresh_token":"REFRESH_TOKEN",
         "openid":"OPENID",
         "scope":"SCOPE" }
        错误时微信会返回JSON数据包如下（示例为code无效错误）:
        {"errcode":40029,"errmsg":"invalid code"}
        */
        $debug_msg_pre = '微信网页授权  刷新授权凭证access_token，';
        $noIdealData = array();
        $result = $this->http_get(self::API_BASE_URL_PREFIX.self::OAUTH_REFRESH_URL.
            'appid='.$this->appId.'&grant_type=refresh_token&refresh_token='.$refresh_token);
        if ($result)
        {
            $json = json_decode($result,true);
            if(  $this->noIdealDataInJSON($json,$debug_msg_pre) === false  )
            {
                return $noIdealData;
            }
            $this->setCache($json, $this->user_token_cache_file,'刷新网页授权access_token');
            $this->user_token = $json['access_token'];
            return $json;
        }
        else
        {
            return $this->noResultAfterHttpRequest( $debug_msg_pre ,$noIdealData );
        }
    }

    /**
     * 微信网页授权  第四步：拉取用户信息(需scope为 snsapi_userinfo)
     * @param string $access_token
     * @param string $openid
     * @return array {openid,nickname,sex,province,city,country,headimgurl,privilege,[unionid]}
     * 注意：unionid字段 只有在用户将公众号绑定到微信开放平台账号后，才会出现。建议调用前用isset()检测一下
     */
    public function getOauthUserInfo($access_token,$openid)
    {
        /*正确时返回的JSON数据包如下：
        {    "openid":" OPENID",
         " nickname": NICKNAME,
         "sex":"1",
         "province":"PROVINCE"
         "city":"CITY",
         "country":"COUNTRY",
         "headimgurl":    "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ
                        4eMsv84eavHiaiceqxibJxCfHe/46",
        "privilege":[ "PRIVILEGE1" "PRIVILEGE2"     ],
         "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"//只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
        }
        错误时微信会返回JSON数据包如下（示例为openid无效）:
        {"errcode":40003,"errmsg":" invalid openid "}
        */
        $debug_msg_pre = '微信网页授权  刷新授权凭证access_token，';
        $noIdealData = array();
        $result = $this->http_get(self::API_BASE_URL_PREFIX.self::OAUTH_USERINFO_URL.
            'access_token='.$access_token.'&openid='.$openid);
        if ($result)
        {
            $json = json_decode($result,true);
            if(  $this->noIdealDataInJSON($json,$debug_msg_pre) === false  )
            {
                return $noIdealData;
            }
            return $json;
        }
        else
        {
            return $this->noResultAfterHttpRequest( $debug_msg_pre ,$noIdealData );
        }
    }
}