<?php
namespace app_common\core\wx\miniApp;

class ByteDanceMiniApp extends MiniApp{
    private string $appId;//【类型：字符串】
    private string $appSecret;//【类型：字符串】
    const wxUserRejectSubscriptionMessage = 43101;

    /**
     * WxMiniApp constructor.
     * @param string $appId
     * @param string $appSecret
     */
    public function __construct(string $appId, string $appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }

    /**
     * 发送订阅消息
     * @param array $data
     * @param string $access_token
     * @return array
     */
    public function sendSubscriptionMessage(array $data,string $access_token){
        $purpose = '发送订阅消息，';
        $url = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token={$access_token}";
        /*属性	类型	默认值	必填	说明
access_token	string		是	接口调用凭证
touser	string		是	接收者（用户）的 openid
template_id	string		是	所需下发的订阅模板id
page	string		否	点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
data	Object		是	模板内容，格式形如 { "key1": { "value": any }, "key2": { "value": any } }
miniprogram_state	string		否	跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
lang	string		否	进入小程序查看”的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN
返回值
Object
返回的 JSON 数据包
接口限制  次数限制：开通支付能力的是3kw/日，没开通的是1kw/日。*/
        $data['access_token'] = $access_token;
        $result = $this->http_post($url,self::json_encode($data));
        $errorCodesMeans = array(
            40003 => 'touser字段openid为空或者不正确',
            40037 => '订阅模板id为空不正确',
            self::wxUserRejectSubscriptionMessage => '用户拒绝接受消息，如果用户之前曾经订阅过，则表示用户取消了订阅关系',
            47003 => '模板参数不准确，可能为空或者不满足规则，errmsg会提示具体是哪个字段出错',
            41030 => 'page路径不正确，需要保证在现网版本小程序中存在，与app.json保持一致'
        );
        return $this->buildResult($result,$purpose,$errorCodesMeans);
    }

    /**
     * 获取access_token【全局唯一票据】
     * @param string $appId 如在类初始化时已提供，则可为空
     * @param string $appSecret 如在类初始化时已提供，则可为空
     * @return array
     */
    public function requestGlobalAccessToken($appId='', $appSecret='')
    {
        /*全局唯一票据，公众号调用各接口时都需使用access_token。
        开发者需要进行妥善保存。access_token的存储至少要保留512个字符空间。
        access_token的有效期目前为2个小时，需定时刷新

        正常情况下，微信会返回下述JSON数据包给公众号：
        {"access_token":"ACCESS_TOKEN","expires_in":7200}
        错误时微信会返回错误码等信息，JSON数据包示例如下（该示例为AppID无效错误）:
        {"errcode":40013,"errmsg":"invalid appid"}
        */
        if ( !$appId || !$appSecret )
        {
            $appId = $this->appId;
            $appSecret = $this->appSecret;
        }
        $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential'.
            "&appid={$appId}&secret={$appSecret}";
        $result = $this->http_get($url);
        return $this->buildResult($result,'全局唯一票据access_token',[]);
    }

    /**
     *通过code换取session_key和 openid
     * @param string $code
     * @return array
     */
    public function requestSessionKeyAndOpenId(string $code)
    {
        /*//正常返回的JSON数据包
            {
                  "openid": "OPENID",
                  "session_key": "SESSIONKEY"
            }
        属性	类型	说明
        openid	string	用户唯一标识
        session_key	string	会话密钥
        unionid	string	用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回。
            //错误时返回JSON数据包(示例为Code无效)
            {
                "errcode": 40029,
                "errmsg": "invalid code"
            }
        */
        $actionMean = '通过code换取session_key和 openid';
        //https://api.q.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code
        $url = "https://api.q.qq.com/sns/jscode2session?appid={$this->appId}".
            "&secret={$this->appSecret}&js_code={$code}&grant_type=authorization_code";
        $result = $this->http_get($url);
        return $this->buildResult($result,$actionMean,[]);
    }

}
