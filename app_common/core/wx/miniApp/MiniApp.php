<?php
namespace app_common\core\wx\miniApp;

class MiniApp {
    use \app_common\core\wx\WxCommon;

    final public static function parseAppId(string $referer){
        $referer = ltrim($referer,'https://');
        if(str_contains($referer,'?') && str_contains($referer,'&')){
            $arr = explode('?', $referer);
            $params = [];
            parse_str($arr[1], $params);
            return $params['appid'];
        }
        /*网络请求的 referer header 不可设置。
        其格式固定为 https://servicewechat.com/{appid}/{version}/page-frame.html，
        其中 {appid} 为小程序的 appid，{version} 为小程序的版本号，
        版本号为 0 表示为开发版、体验版以及审核版本，
        版本号为 devtools 表示为开发者工具，
        其余为正式版本；*/
        $arr = explode('/',$referer);
        return $arr[1];
    }

    final public static function isFromQQMiniApp(string $referer){
        return strpos($referer,'https://appservice.qq.com') === 0;
    }

    final public static function isFromWxMiniApp(string $referer){
      return strpos($referer,'https://servicewechat.com') === 0;
    }

    final public static function isFromByteDanceMiniApp(string $referer){
        return strpos($referer,'https://tmaservice.developer.toutiao.com') === 0;
    }

}
