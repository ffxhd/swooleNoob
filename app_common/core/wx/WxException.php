<?php
namespace app_common\core\wx;

use Throwable;

class WxException extends \Exception
{
    public function __construct($message = "", $code = -1, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}