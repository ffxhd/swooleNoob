<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/21
 * Time: 13:40
 */
namespace app_common\core\session;
class SessionRedis
{

    /**
     * @throws \Exception
     */
    private function connectRedis(): \Redis
    {
        $redis = new \Redis();
        $redisConfig = APP_CONFIG['redis'];
        $redis->connect($redisConfig['host'],$redisConfig['port']);
        $password = $redisConfig['password'];
        if($password !== ''){
            $result = $redis->auth($password);
            if( $result === false){
                throw new \Exception('连接redis成功，验证密码失败');
            }
        }
        $redis->select($redisConfig['database']);
        return $redis;
    }

    private function fillWithPackName(string $sessionId):string
    {
        $packName = APP_CONFIG['session']['redis']['packName'];
        return "{$packName}:{$sessionId}";
    }

    /**
     * @param string $sessionId
     * @return array
     * @throws \Exception
     */
    public function read(string $sessionId ):array
    {
        $redis = $this->connectRedis();
        $sessionId = $this->fillWithPackName($sessionId);
        //say('$session_id',$session_id);
        $sessionData = $redis->get($sessionId);//获取redis中的指定记录
        //say('$session_data',$session_data);
        $redis->close();
        if( $sessionData === false )
        {
           return  [];
        }
        return json_decode($sessionData,true);
    }

    /**
     * @param string $sessionId
     * @param array $sessionData
     * @throws \Exception
     */
    public function write(string $sessionId , array $sessionData)
    {
        $redis = $this->connectRedis();
        $sessionId = $this->fillWithPackName($sessionId);
        $sessionData = json_encode($sessionData, JSON_UNESCAPED_UNICODE);
        $result = $redis->set($sessionId,$sessionData);
        if( true === $result)
        {
           $expireTime = \app_common\core\session\SessionFactory::getExpireTime();
           $redis->expire($sessionId,$expireTime);
        }
        $redis->close();
    }

    /**
     * @param $sessionId
     * @throws \Exception
     */
    public function remove($sessionId){
        $redis = $this->connectRedis();
        $sessionId = $this->fillWithPackName($sessionId);
        $redis->del($sessionId);
        $redis->close();
    }
}
