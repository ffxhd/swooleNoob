<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/22
 * Time: 19:48
 */

namespace app_common\core\session;
class SessionFactory
{
    private static ?SessionRedis $manager = null;
    public function __construct()
    {
        self::$manager = new  \app_common\core\session\SessionRedis();
    }

    public static function getUniqueSessionId(int $fd, int $requestTime):string
    {
        return uniqid("fd_{$fd}_{$requestTime}");
    }

    /**
     * @param string $sessionId
     * @return array
     * @throws \Exception
     */
    public static function read(string $sessionId):array
    {
        return self::$manager->read($sessionId);
    }

    /**
     * @param string $sessionId
     * @param array $sessionData
     * @throws \Exception
     */
    public  static function write(string $sessionId , array $sessionData )
    {
        self::$manager->write($sessionId,$sessionData);
    }

    /**
     * @param string $sessionId
     * @throws \Exception
     */
    public static function remove(string $sessionId){
        self::$manager->remove($sessionId);
    }

    public static function getFutureTime(int $requestTime):int
    {
        return $requestTime + self::getExpireTime();
    }

    public static function getExpireTime()
    {
        return APP_CONFIG['session']['expireTime'];
    }

    public static function getSessionKey():string
    {
        return APP_CONFIG['session']['name'];
    }
}
