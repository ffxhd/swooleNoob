<?php
/* PHP SDK
 * @version 2.0.0
 * @author connect@qq.com
 * @copyright © 2013, Tencent Corporation. All rights reserved.
 */
namespace app_common\core\QQ_Connect;
use app_common\core\QQ_Connect\URL;
use app_common\core\QQ_Connect\ErrorCase;

class Oauth{

    const VERSION = '2.0';
    const GET_AUTH_CODE_URL = 'https://graph.qq.com/oauth2.0/authorize';
    const GET_ACCESS_TOKEN_URL = 'https://graph.qq.com/oauth2.0/token';
    const GET_OPENID_URL = 'https://graph.qq.com/oauth2.0/me';

    protected $recorder;
    public $urlUtils;
    protected $error;
    private string $appId;
    

    function __construct($appId){
        $this->appId = $appId;
        //$this->recorder = new Recorder();
        $this->urlUtils = new URL();
        $this->error = new ErrorCase();
    }

    public function buildStateString(){
        //-------生成唯一随机串防CSRF攻击
        return md5(uniqid(rand(), TRUE));
    }

    public function qq_login(string $callback, string $scope, string $state){
        //-------构造请求参数列表
        $keysArr = array(
            'response_type' => 'code',
            'client_id' => $this->appId,
            'redirect_uri' => $callback,
            'state' => $state,
            'scope' => $scope
        );

        return $this->urlUtils->combineURL(self::GET_AUTH_CODE_URL, $keysArr);
    }

    /**
     * @param string $callback
     * @param string $appKey
     * @param string $code
     * @param string $stateFromSession
     * @param string $stateFromRequest
     * @return mixed
     * @throws \Exception
     */
    public function requestAccessToken(string $callback, string $appKey, string $code, string $stateFromSession, string $stateFromRequest){
        //--------验证state防止CSRF攻击
        if(!$stateFromSession || $stateFromRequest !== $stateFromSession){
            $this->error->showError('30001');
        }
        //-------请求参数列表
        $keysArr = array(
            'grant_type' => 'authorization_code',
            'client_id' => $this->appId,
            'redirect_uri' => urlencode($callback),
            'client_secret' => $appKey,
            'code' => $code,
        );

        //------构造请求access_token的url
        $token_url = $this->urlUtils->combineURL(self::GET_ACCESS_TOKEN_URL, $keysArr);
        $response = $this->urlUtils->get_contents($token_url);

        if(strpos($response, 'callback') !== false){

            $lpos = strpos($response, '(');
            $rpos = strrpos($response, ')');
            $response  = substr($response, $lpos + 1, $rpos - $lpos -1);
            $msg = json_decode($response);

            if(isset($msg->error)){
                $this->error->showError($msg->error, $msg->error_description);
            }
        }

        $params = array();
        parse_str($response, $params);
        return $params;
    }

    /**
     * @param  string $accessToken
     * @return string
     * @throws \Exception
     */
    public function requestOpenid(string $accessToken){
        //-------请求参数列表
        $keysArr = array(
            'access_token' => $accessToken
        );

        $graph_url = $this->urlUtils->combineURL(self::GET_OPENID_URL, $keysArr);
        $response = $this->urlUtils->get_contents($graph_url);

        //--------检测错误是否发生
        if(strpos($response, 'callback') !== false){

            $lpos = strpos($response, '(');
            $rpos = strrpos($response, ')');
            $response = substr($response, $lpos + 1, $rpos - $lpos -1);
        }

        $user = json_decode($response);
        if(isset($user->error)){
            $this->error->showError($user->error, $user->error_description);
        }

        return $user->openid;
    }
}
