<?php
namespace app_common\core\file;
use app_common\core\wpfString;

class Upload
{
    private int    $maxSize = 0;
    private string $httpPath = '';
    private string $path = '';
    private array  $allowExt = [];

    public function __construct(array $uploadConfig)
    {
        if( 0 > 1){
            $uploadConfig = [
                'httpPath' => '',
                'path' => '',
                'allowExt' => [],
                'maxSize' => 0,
            ];
        }
        $this->httpPath= getItemFromArray($uploadConfig,'httpPath');
        $this->path = getItemFromArray($uploadConfig,'path','uploads');
        //允许的扩展名
        $this->allowExt = getItemFromArray($uploadConfig,'allowExt',['gif','jpeg','png','jpg','wbmp']);
        //允许的文件最大大小
        $this->maxSize = getItemFromArray($uploadConfig,'maxSize',2 * 1024 * 1024);
    }

    /**
     * 上传多文件或(和)多个单文件
     * @param array $FILES
     * @param boolean $isSwoole
     * @return array|string 成功上传的文件组的名称和错误信息
     */
    public function uploadFile($FILES,bool $isSwoole)
    {
        $isExifExtensionLoaded = extension_loaded('exif');
        $noticeArray = [];
        $path = $this->path;
        $allowExt = $this->allowExt;
        $maxSize = $this->maxSize;
        $files = self::buildInfo( $FILES );
        //say('处理后的文件信息：',$files);
        if( true === empty($files))
        {
            return '文件信息为空';
        }
        if( false === is_array($files) ){
            return '文件信息不是一个数组';
        }
        $i = 0;
        $uploadedFiles = array();
        if(false === is_dir($path))
        {
            mkdir($path,0777,true);	//创建文件夹
        }
        foreach($files as $file)
        {
            if($file['error'] !== UPLOAD_ERR_OK){
                switch($file['error'])
                {
                    case UPLOAD_ERR_INI_SIZE:
                        $noticeArray[] = '超过了配置文件上传文件的大小<br/>';
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        $noticeArray[] = '超过了表单设置上传文件的大小<br/>';
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        $noticeArray[] = '文件部分被上传<br/>';
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        $noticeArray[] = '没有文件被上传<br/>';//
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        $noticeArray[] = '没有找到临时目录<br/>';//
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        $noticeArray[] = '文件不可写<br/>';//;
                        break;
                    case  UPLOAD_ERR_EXTENSION :
                        $noticeArray[] = '由于PHP的扩展程序中断了文件上传<br/>';
                        break;
                    default:
                        $noticeArray[] = '由于未知错误无法完成文件上传<br/>';
                }
                continue;
            }
            $toUploadFileName = $file['name'];
            $ext = wpfString::getExt($toUploadFileName);
            //检测文件的扩展名
            if( false === in_array($ext,$allowExt))
            {
                //$notice = $file['name'].'是非法文件类型<br/>';
                $noticeArray[] = $toUploadFileName.'是非法文件类型';
                //$imgFlag = false;
                continue;
            }
            //上传文件的大小
            if($file['size'] > $maxSize)
            {
                $noticeArray[] = '上传文件'.$toUploadFileName.'过大';
                continue;
            }
            $temporaryFile = $file['tmp_name'];
            if( false === $isSwoole){
                //校验是否是一个真正的图片类型
                $isImageFile = true === $isExifExtensionLoaded ? @exif_imagetype( $temporaryFile) : @getimagesize($temporaryFile);
                if( $isImageFile === false )
                {
                    $noticeArray[] = $toUploadFileName.'不是真正的图片类型';
                    continue;
                }
                if(false === $isSwoole && false === is_uploaded_file($temporaryFile))
                {
                    $noticeArray[] = $toUploadFileName.'不是通过HTTP POST方式上传上来的';
                    continue;
                }
                $filename = wpfString::getUniName().'.'.$ext;
                $destination = $path.'/'.$filename;
                if(false === move_uploaded_file($temporaryFile, $destination))
                {
                    $noticeArray[] = $toUploadFileName.'移动到目录文件夹失败';
                    continue;
                }
            }
            else{
                $filename = wpfString::getUniName().'.'.$ext;
                $destination = $path.'/'.$filename;
                if(false === rename($temporaryFile, $destination))
                {
                    $noticeArray[] = $toUploadFileName.'移动到目标文件夹失败';
                    continue;
                }
                $isImageFile = true === $isExifExtensionLoaded ? @exif_imagetype( $destination) : @getimagesize($destination);
                if( $isImageFile === false )
                {
                    unlink($destination);
                    $noticeArray[] = $toUploadFileName.'不是真正的图片';
                    continue;
                }
            }
            unset($file['name'], $file['tmp_name'], $file['size'], $file['type'], $file['error'] );
            $uploadedFiles[$i] = [
                'url' => $this->httpPath.'/'.$filename,
                'path' => $destination,
            ];
            $i++;
        }
        return [
            'uploadedFiles' => $uploadedFiles,
            'notices' => $noticeArray,
        ];
    }

    private function uploadTraditionally(){

    }

    /**构建文件上传信息
	 * @param $FILES
	 * @return array
	 */
	private static function buildInfo($FILES)
	{
		if(!$FILES)
		{
			return array() ;
		}//'没有文件上传,故没有文件信息'
		$i=0;
		$files = array();
		//如果是多个单文件
        $fileInfo_0 = getItemFromArray($FILES,0,null);
        if( $fileInfo_0 === null)
        {
            $fileName = getItemFromArray($FILES,'name','');
            if($fileName !== '')
            {
                return [$FILES];
            }
            foreach ($FILES as $key => $Files)
            {
                $files[$i]= $Files;
                $i++;
            }
            return $files;
        }
        //
        foreach ($FILES as $key => $Files)
        {
            $files[$i]['name'] = $Files['name'];
            $files[$i]['size']= $Files['size'];
            $files[$i]['tmp_name']=$Files['tmp_name'];
            $files[$i]['error']=$Files['error'];
            $files[$i]['type']=$Files['type'];
            $i++;
        }
        return $files;
	}

	private function oldBuildFileInfo($FILES){
        if(!$FILES)
        {
            return array() ;
        }//'没有文件上传,故没有文件信息'
        $i=0;
        $files = array();
        //旧代码
        foreach($FILES as $Files)//$_FILES三维数组 as $Files二维数组
        {
            if(is_string($Files['name']))//如果是单文件
            {
                $files[$i]=$Files;
                $i++;
            }
            else //多文件
            {
                foreach($Files['name'] as $key=>$val)
                {
                    $files[$i]['name']=$val;
                    $files[$i]['size']=$Files['size'][$key];
                    $files[$i]['tmp_name']=$Files['tmp_name'][$key];
                    $files[$i]['error']=$Files['error'][$key];
                    $files[$i]['type']=$Files['type'][$key];
                    $i++;
                }
            }
        }
        //printr($files);exit;
        return $files;
    }
}
