<?php
namespace app_common\core\form;
class Validation
{
    public static function test()
    {
        $config = [
            'is_public' =>[
                'mean' => '是否公开',
                'idealType' =>'number',
                'require'=>true,
                'mustBeInteger'=> true,
                'forbid'=>[
                    0 =>'参数为0，不知是要点赞还是取消点赞...'
                ],
            ],
            'id' =>[
                'mean' => 'id',
                'idealType' =>'number',
                'require'=>true,
                'mustBeInteger'=> true,
                'range'=>'(0,+)',// 0 < x
            ],
            'f' =>[
                'mean' => 'f',
                'idealType' =>'number',
                'require'=>true,
                'mustBeInteger'=> true,
                'range'=>'(-,0)',//x < 0
            ],
            'money' =>[
                'mean' => '提现金额',
                'idealType' =>'number',
                'require'=>true,
                'mustBeInteger'=> false,
                'range'=>'(30,120)',//30 < x < 120
            ],
            'age' =>[
                'mean' => '年纪',
                'idealType' =>'number',
                'require'=>true,
                'mustBeInteger'=> true,
                'range'=>'(0,120]',//0 < x ≤ 120
            ],
            'percent' =>[
                'mean' => '比例',
                'idealType' =>'number',
                'require'=>true,
                'mustBeInteger'=> false,
                'range'=>'[0,100]',//0 ≤ x ≤ 1
            ],
            'its_type' =>[
                'mean' => '指定类型',
                'idealType' =>'string',
                'forbidType'=>'number',
                'require'=>true,
                'mustIn'=>[
                    '英雄联盟','王者荣耀',
                ]
            ],
        ];
        $source = $_GET;
        //say('$source',$source);
        $result = self::validate($source, $config);
        if(false === is_array($result)){
            return  "api结构：不符合规则: {$result}";
        }
        return $result;//合适的数据，可以用于插入或者更新
    }

    public static function validate($source, $config)
    {
        $data = [];
        foreach($config as $field => $item)
        {
            $value = getItemFromArray($source,$field,'');
            $value = trim($value);
            if(true === $item['require'])
            {
                //验证是否有值
                if($value === '')
                {
                    return "{$item['mean']}不能为空，参数---{$field}";
                }
            }
            $explain = $item['mean'];
            switch($item['idealType'])
            {
                case 'number':
                    $result =self::checkNumber($item,$value,$explain);
                    if(false === is_numeric($result))
                    {
                        return $result;
                    }
                    $data[$field] = $result;
                    break;
                case "string":
                    $result =self::checkStr($item,$value,$explain);
                    if(true === $result)
                    {
                        $data[$field] = $value;
                    }
                    else
                    {
                        return $result;
                    }
                    break;
                case 'preg':
                    $result =self::checkPreg($item,$value,$explain);
                    if(true === $result)
                    {
                        $data[$field] = $value;
                    }
                    else
                    {
                        return $result;
                    }
                default:
            }
        }
        return $data;
    }

    protected static function checkStr($item,$value,$explain)
    {
        $forbidType = getItemFromArray($item,'forbidType','');
        if( $forbidType !== '')
        {
            if(true === is_numeric($value))
            {
                return $explain.'不能是数字';
            }
        }
        $mustIn = getItemFromArray($item,'mustIn',[]);
        if( false === empty($mustIn))
        {
            if( false === in_array($value,$mustIn))
            {
                $specifyValues = implode(' , ',$mustIn);
                return "{$explain}必须是指定值之一：{$specifyValues}";
            }
        }
        return true;
    }

    protected static function checkNumber($item,$value,$explain)
    {
        $range = getItemFromArray($item,'range','');
        if(false === is_numeric($value))
        {
            return  $explain.'必须是数字';
        }
        //
        $mustBeInteger = getItemFromArray($item,'mustBeInteger',false);
        /*if( true === $mustBeInteger)
        {
            if( false === is_int($value))
            {
                return  $explain.'必须是整数';
            }
            $value = intval($value);
        }*/
        //
        $forbid = getItemFromArray($item,'forbid',[]);
        if(false === empty($forbid))
        {
            $value = intval($value);
            foreach($forbid as $forbidValue => $msg)
            {
                //say('$value',$value,'$forbidValue',$forbidValue);
                if($value === $forbidValue)
                {
                    return $msg;
                }
            }
        }
        //
        if('' === $range)
        {
            return $value;
        }
        $value = floatval($value);
        $arr = explode(',',$range);
        $left =substr( $arr[0],0,1);
        $min = substr( $arr[0],1);
        $right = substr( $arr[1],-1,1);
        $max = substr( $arr[1],0,-1);
        //say('$range',$range,'$left',$left,'$right',$right,'$min',$min,'$max',$max);
        if( true === is_numeric($min))
        {
            $min = true === is_integer($min) ? intval($min) : floatval($min);
            switch($left)
            {
                case '(':
                    if(!($value > $min))
                    {
                        return $explain.'必须大于'.$min;
                    }
                    break;
                case '[':
                    if(!($value >= $min))
                    {
                        return $explain.'必须大于等于'.$min;
                    }
                    break;
                default:
            }
        }
        if( true === is_numeric($max))
        {
            $max =  true === is_integer($max) ? intval($max) : floatval($max);
            switch($right)
            {
                case ')':
                    if(!($value < $max))
                    {
                        return $explain.'必须小于'.$max;
                    }
                    break;
                case ']':
                    if(!($value <= $max))
                    {
                        return $explain.'必须小于等于'.$max;
                    }
                    break;
                default:
            }
        }
        return $value;
    }

    protected static function checkPreg($value,$pregExp,$explain)
    {
        if(false === preg_match($pregExp,$value ))
        {
            return $explain.'格式不正确';
        }
        else
        {
            return true;
        }
    }
}
