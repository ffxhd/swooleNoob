<?php
namespace app_common\core\form;
class MyDateTime{

    /**
     * 根据指定时间戳算出那一天的起止时间戳
     * @param integer $timestamp
     * @param bool $needString
     * @return array|string
     */
    public static  function startEndTimestampOfTheDay(int $timestamp, bool $needString=true): array|string
    {
        // $numArr = array(1,2,3,4,5,6,7,8,9,10,11,12);
        //$chineseNumArr = array('一', '二', '三', '四', '五','六', '七', '八', '九', '十', '十一', '十二');
        $todayDate  = date('Y-m-d ',$timestamp);
        $todayStart = date_create($todayDate.' 00:00:00');
        $todayEnd   = date_create($todayDate.' 23:59:59');
        $todayStartSpan =  date_timestamp_get($todayStart);  //今天0:00:00的时间戳
        $todayEndSpan   =  date_timestamp_get($todayEnd);    //今天23:59:59的时间戳
        return $needString? $todayStartSpan.' and '.$todayEndSpan : array( $todayStartSpan, $todayEndSpan );
    }

    /**
     * 根据指定的日期，计算此后n天的连续的日期数组
     * @param string $startDate
     * @param int $several
     * @return array
     */
    public static function buildConsecutiveDatesArray(string $startDate, int $several ): array
    {
        $dates = [];
        $dates[] = $startDate;
        $arr = explode('-', $startDate);
        $year  = intval($arr[0]);
        $month = intval($arr[1]);
        $day   = intval($arr[2]);
        $lastDay = self::calculateLastDay($year, $month);
        for($i = 1; $i < $several; $i++){
            $newDay   = $day + $i;
            $newMonth = $month;
            $newYear  = $year;
            if( $newDay > $lastDay ){
                $newMonth = $month + 1;
                $newDay -= $lastDay;
            }
            if( $newMonth > 12 ){
                $newYear = $year + 1;
            }
            $newMonth = self::fillWith_0_orNot($newMonth);
            $newDay  = self::fillWith_0_orNot($newDay);
            $dates[] = "{$newYear}-{$newMonth}-{$newDay}";
        }
        return $dates;
    }

    /**
     * 根据指定的日期，计算n天后的日期
     * @param string $todayDate
     * @param int $several
     * @return string
     */
    public static function calculateFutureDate(string $todayDate, int $several): string
    {
        $arr = explode('-', $todayDate);
        $year  = intval($arr[0]);
        $month = intval($arr[1]);
        $date  = intval($arr[2]);
        $lastDay = self::calculateLastDay($year, $month);
        $date += $several;
        if( $date > $lastDay ){
            $month ++;
            $date -= $lastDay;
        }
        if( $month > 12 ){
            $year ++;
        }
        $month = self::fillWith_0_orNot($month);
        $date  = self::fillWith_0_orNot($date);
        return "{$year}-{$month}-{$date}";
    }

    /**
     * 根据指定年月，计算最后一天的数字
     * @param int $year
     * @param int $month
     * @return int
     */
    public static function calculateLastDay(int $year, int $month): int
    {
        $month31d = array(1, 3, 5, 7, 8, 10, 12);
        $month30d = array(4, 6, 9, 11);
        if( true === in_array($month, $month31d) ){
            return 31;
        }
        if( true === in_array($month, $month30d) ){
            return 30;
        }
        return $year % 4 === 0 ? 29 : 28;
    }

    /**
     * 根据指定时间戳，算出那一个月的起止日期
     * @param integer $timestamp 时间戳
     * @param bool $needString
     * @return array|string 例如["2019-02-01","2019-02-28"]
     */
    public static  function dateRangeOfTheMonth(int $timestamp, bool $needString=true): array|string
    {
        $Ym = date('Y-m',$timestamp);
        $arr = explode('-',$Ym);
        $year  = intval($arr[0]);
        $month = intval($arr[1]);
        $lastDay = self::calculateLastDay($year, $month);
        $startDay  = $year.'-'.$month.'-01';
        $endDay    = $year.'-'.$month.'-'.$lastDay;
        return $needString ? $startDay.' and '.$endDay : array($startDay,$endDay);
    }

    /**
     * 本月的每一天以及对应星期几
     * @param string $startDay 例如 2019-04-10
     * @param string $lastDay
     * @return array [1=>0,2=>1...31=>4]//日期=>周几(0表示周一)
     */
    public static  function thisMonth_eachDayWithWeek(string $startDay, string $lastDay): array
    {
        //本月的每一天对应星期几？
        $day_week = array();
        //本月第一天是星期几
        $startDay     = date_create($startDay);
        $startDaySpan = date_timestamp_get($startDay);  //本月第一天的时间戳
        $firstWeekNum = date('w',$startDaySpan);//0表示周日，1表示周一，2表示周二，...，6表示周六
        //留白
        if($firstWeekNum >= 1)
        {
            for($i = 0; $i < $firstWeekNum; $i ++)
            {
                $day_week[-10-$i] = null;
            }
        }
        //本月的每一天对应星期几
        $theWeekNum = $firstWeekNum;
        for($i = 1; $i <= $lastDay; $i ++)
        {
            $day_week[$i] = $theWeekNum;
            $theWeekNum++;
            $theWeekNum = $theWeekNum === 7 ? 0 : $theWeekNum;
        }
        return $day_week;
    }

    /**
     * 比较时间的大小（人类容易阅读的格式，例如：2019-04-10 21:07:00）
     * @param string $dateTime 例如 2019-04-10 21:07:00
     * @param string $dateTime2 例如 2019-04-10 22:07:00
     * @return string
     */
    public static  function compareTimeSize(string $dateTime, string $dateTime2): string
    {
        $a = strtotime($dateTime);
        $b = strtotime($dateTime2);
        return match (true) {
            $a > $b => '>',
            $a < $b => '<',
            default => '=',
        };
    }

    /**
     * 计算时间差
     * @param string $dateTime
     * @param string $dateTime2
     * @return string
     */
    public static function getTimeDifference(string $dateTime, string $dateTime2): string
    {
        $date1 = date_create($dateTime);
        $date2 = date_create($dateTime2);
        $diff = date_diff($date1,$date2);
        return $diff->format("%Y:%M:%D %H:%I:%S");
    }

    /**
     * 获取过去的时间
     * @param string $fromTime
     * @param string $dateString
     * @return false|string
     */
    public static function getPastDateTime(string $fromTime,string $dateString): false|string
    {
        try {
            $dateTime = new \DateTime($fromTime);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        $interval = \DateInterval::createFromDateString($dateString);
        $theDate = $dateTime->sub($interval);
        return date_format($theDate,'Y-m-d H:i:s');
    }
    //======================================================================

    /**
     * 根据年月日，返回那天是周几
     * @param int $y
     * @param int $m
     * @param int $d
     * @return false|int|string
     */
    public static function getWeek(int $y = 0, int $m = 0, int $d = 0): false|int|string
    {
        $obj = date_create("{$y}-{$m}-{$d}");
        $timestamp = date_timestamp_get($obj);
        $w = date('w',$timestamp);
        $w = intval($w);
        return $w === 0 ? 7 : $w;
    }

    /**
     * 返回当前月份
     * @return int
     */
    public static function getMonthAsInteger(): int
    {
        return intval(date('n'));
    }

    /**
     * 返回当前年份
     * @return int
     */
    public static function getFullYear(): int
    {
        return intval(date('Y'));
    }

    /**
     * 返回指定年月的最后一天的数字 -- 28,29,30,31
     * @param int $m
     * @param int $y
     * @return int
     */
    public static function getLastDayOfTheMonth(int $m = 0, int $y = 0): int
    {
        $m = $m === 0 ? self::getMonthAsInteger() : $m;
		if( $m === 2)
        {
            $y = $y === 0 ? self::getFullYear() : $y;
            return $y % 4 === 0 ? 29 : 28;
        }
        $d30 = [ 4,6,9,11];
        return true === in_array($m,$d30) ? 30 : 31;
	}

	private static function fillWith_0_orNot($s)
    {
        if( $s < 10 )
        {
            $s = '0' .$s;
        }
        return $s;
    }

	private static function formatYmd($theMonth, $wd, $y): string
    {
        $theMonth = self::fillWith_0_orNot($theMonth);
        $wd = self::fillWith_0_orNot($wd);
        return "{$y}-{$theMonth}-{$wd}";
    }

    /**
     * 返回指定年月日所在的那一星期的起止日期
     * @param int $y
     * @param int $m
     * @param int $d
     * @return array
     */
    public static function getDateRangeOfTheWeek(int $y = 0, int $m = 0, int $d = 0): array
    {
        if($y === 0 && $m === 0 && $d === 0)
        {
            $date = date('Y-n-j-w',$_SERVER['REQUEST_TIME']);
            $arr = explode('-',$date);
            $y = $arr[0];
            $m = $arr[1];
            $d = $arr[2];
            $w = $arr[3];
        }
        else
        {
            $w = self::getWeek($y, $m, $d);
        }
        //say("$y-$m-$d,周{$w}");
		//本周第一天  本周最后一天
		$w1_d = $d + ( 1 - $w );// 负数 或者 0
		$w7_d = $d + ( 7 - $w );//正数
		//本周第一天是否跨月
		if( $w1_d <= 0)
        {
            $lastMonth = $m - 1;
			if( $lastMonth < 1 )
            {
                $lastMonth = 12;
                $y--;
            }
            $lastDay_theMonth = self::getLastDayOfTheMonth($lastMonth,$y);
            $w1_d = $lastDay_theMonth + $w1_d;
            $w1_d = self::formatYmd($lastMonth,$w1_d,$y);
		}
        else
        {
            $w1_d = self::formatYmd($m,$w1_d,$y);
        }
		//本周最后一天是否跨月 是否跨年
        $lastDay_thisMonth = self::getLastDayOfTheMonth($m,$y);
		if( $w7_d > $lastDay_thisMonth )
        {
            $nextMonth = $m + 1;
			if( $nextMonth > 12 )
            {
                $nextMonth = 1;
                $y ++;
            }
            $w7_d = $w7_d - $lastDay_thisMonth;
            $w7_d = self::formatYmd($nextMonth,$w7_d,$y);
		}
        else
        {
            $w7_d = self::formatYmd($m,$w7_d,$y);
        }
		return [$w1_d, $w7_d];
	}

    public static function getEachDateOfTheWeek($y = 0, $m = 0, $d = 0): array
    {
        if($y === 0 && $m === 0 && $d === 0)
        {
            $date = date('Y-n-j-w',$_SERVER['REQUEST_TIME']);
            $arr = explode('-',$date);
            $y = $arr[0];
            $m = $arr[1];
            $d = $arr[2];
            $w = $arr[3];
        }
        else
        {
            $w = self::getWeek($y, $m, $d);
        }
        //say("$y-$m-$d,周{$w}");
        //本周第一天  本周最后一天
        $w1_d = $d + ( 1 - $w );// 负数 或者 0
        $w7_d = $d + ( 7 - $w );//正数
        //say('$w1_d',$w1_d,'$w7_d',$w7_d);
        $weekDays = [];
        //
        $lastDay_thisMonth = self::getLastDayOfTheMonth($m,$y);
        //say('$lastDay_thisMonth',$lastDay_thisMonth);
        //
        for($wNd = $w1_d; $wNd <= $w7_d; $wNd ++ )
        {
            $wNy = $y;
            $wNm = $m;
            $wNd2 = $wNd;
            if( $wNd > $lastDay_thisMonth )
            {
                $wNm = $m + 1;
                if( $wNm > 12 )
                {
                    $wNm = 1;
                    $wNy = $y + 1;
                }
                $wNd2 = $wNd - $lastDay_thisMonth;
            }
            else if( $wNd <= 0)
            {
                $wNm = $m - 1;
                if( $wNm < 1 )
                {
                    $wNm = 12;
                    $y--;
                }
                $lastDay_theMonth = self::getLastDayOfTheMonth($wNm,$y);
                $wNd2 = $lastDay_theMonth + $wNd;
            }
            $weekDays[] = self::formatYmd($wNm,$wNd2,$wNy);
        }
        return $weekDays;
    }

	public static function getEachDayOfTheWeek($y = 0, $m = 0, $d = 0)
    {
        $arr = self::getDateRangeOfTheWeek($y, $m, $d);
        $header = $arr[0];
        $tail = $arr[1];
        //
        $headerArr = explode($header,'-');
        $header_y = $headerArr[0];
        $header_m = $headerArr[1];
        $header_d = $headerArr[2];
        //
        $tailArr = explode($header,'-');
        $tail_y = $headerArr[0];
        $tail_m = $headerArr[1];
        $tail_d = $headerArr[2];
    }
}
