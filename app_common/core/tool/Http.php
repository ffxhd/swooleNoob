<?php

namespace app_common\core\tool;

class Http
{
    /**
     * GET 请求
     * @param string $url
     * @return string|false
     */
    public static function curl_get(string $url): string|false
    {
        $oCurl = curl_init();
        if(stripos($url,'https://')!== false)
        {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        return intval($aStatus['http_code']) === 200 ? $sContent : false;
    }

    /**
     * POST 请求
     * @param string $url
     * @param array|string $param
     * @param boolean $post_file 是否文件上传
     * @return false|string content
     */
    public static function curl_post(string $url, string|array $param, bool $post_file=false): string|false
    {
        $oCurl = curl_init();
        if(stripos($url,'https://') !== FALSE)
        {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }
        if (PHP_VERSION_ID >= 50500 && class_exists('\CURLFile'))
        {
            $is_curlFile = true;
            //$this->searchBugMsgArr[] = '$is_curlFile = true;';
        }
        else
        {
            $is_curlFile = false;
            //$this->searchBugMsgArr[] = '$is_curlFile = false';
            if (defined('CURLOPT_SAFE_UPLOAD'))
            {
                // $this->searchBugMsgArr[] = 'defined(\'CURLOPT_SAFE_UPLOAD\')';
                curl_setopt($oCurl, CURLOPT_SAFE_UPLOAD, false);
            }
        }
        if (is_string($param))
        {
            $strPOST = $param;
        }
        elseif($post_file)
        {
            if($is_curlFile)
            {
                foreach ($param as $key => $val)
                {
                    if (substr($val, 0, 1) == '@')
                    {
                        $param[$key] = new \CURLFile(realpath(substr($val,1)));
                    }
                }
            }
            $strPOST = $param;
        }
        else
        {
            $aPOST = array();
            foreach($param as $key=>$val)
            {
                $aPOST[] = $key.'='.urlencode($val);
            }
            $strPOST =  join('&', $aPOST);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($oCurl, CURLOPT_POST,true);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST);
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        return intval($aStatus['http_code']) === 200 ? $sContent : false;
    }
}