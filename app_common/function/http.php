<?php
/**
 * Created by PhpStorm.
 * User: kuf
 * Date: 2018/10/26
 * Time: 19:19
 */

function getWashedData($source,$field,$default='')
{
    $value = getItemFromArray($source,$field,$default);
    if( true === is_string($value))
    {
        $value = trim($value);
    }
    return addslashes($value);
}

function set_url( $controller,$method,$extraParam = array() )
{
    $base_url = readConfig( 'base_url' );
    return "http://{$base_url}/index.php/{$controller}/{$method}";
}

function joinUrlParamsAsStr( $paramArr )
{
    $params = array();
    foreach( $paramArr as $field => $value )
    {
        $params[] = "{$field}={$value}";
    }
    $str = implode('&',$params);
    return $str;
}

//扩展，想取得$post['a']['b']['c'],任意层级=>$field = a[b][c]或者[a][b][c]
function https_rawData_by_POST($field = '' )
{
    $post = file_get_contents('php://input');//这样才可以获得https的post
    $post = json_decode($post,true);
    if(!is_array($post) )
    {
        return array();
    }
    if( $field )
    {
        if( strpos($field,'[')===false && strpos($field,'[')=== false )
        {
            return  isset($post[$field])? $post[$field] : null;
        }
        else
        {

        }
    }
    else
    {
        return $post;
    }
}

//扩展，想取得$post['a']['b']['c'],任意层级=>$field = a[b][c]或者[a][b][c]
function https_rawData_by_GET($field = '' )
{
    $data = $_GET;
    foreach($data as $k =>$mix)
    {
        if( is_string($mix))
        {
            if( strpos($mix,'{') === 0 && strpos($mix,'}')!== false )
            {
                $data[$k] = json_decode($mix,true);
            }
        }
    }
    if( $field )
    {
        if( strpos($field,'[')===false && strpos($field,'[')=== false )
        {
            return  isset($data[$field])? $data[$field] : null;
        }
        else
        {

        }
    }
    else
    {
        return $data;
    }
}
