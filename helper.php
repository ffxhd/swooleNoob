<?php
/**
 * @example php helper.php -a demo 获得目录app_demo，然后可以开始开发了
 */
const ROOT = __DIR__;
require ROOT.'/must/develop/debug.php';
require ROOT.'/must/Autoload.php';
class helper{
    public function renameLogs(string $applicationName){
        if($applicationName === 'common'){
            exit('不许打app_common目录的主意'.PHP_EOL);
        }
        $fullApplicationName = 'app_'.$applicationName;
        $basicPath = ROOT.'/'.$fullApplicationName;
        $arr = [
            'swooleHttpLogs',
            'swooleWebSocketLogs',
        ];
        $date = date('Ymd');
        foreach ($arr as $value){
            $path = "{$basicPath}/data/{$value}";
            $files = scandir($path);
            say('$files',$files);
            unset($files[0],$files[1]);
            foreach($files as $file)
            {
                $arr = explode('.',$file);
                if(count($arr) === 2){
                    continue;
                }
                $extName = &$arr[2];
                if($extName === $date){
                    continue;
                }
                if($extName === 'html'){
                    continue;
                }
                $oldFileName = "{$path}/{$file}";
                $newFileName = "{$path}/{$arr[0]}.$arr[2].html";
                say("{$oldFileName}->{$newFileName}");
                rename($oldFileName, $newFileName);
            }
        }
    }

    public function  run(string $applicationName){
        if($applicationName === 'common'){
            exit('不许打app_common目录的主意'.PHP_EOL);
        }
        $fullApplicationName = 'app_'.$applicationName;
        $basicPath = ROOT.'/'.$fullApplicationName;
        if( false === is_dir($basicPath)){
            mkdir($basicPath);
            chmod($basicPath, 0777);
        }
        $this->fillDirectories($basicPath);
        $this->fillPhpConfigFiles($basicPath);
        $this->fillServerIni($basicPath);
        $this->fillWebSocketClasses($basicPath);
        $this->fillMainWorkerFile($basicPath, $fullApplicationName);
        $this->fillWorkersFile($basicPath, $fullApplicationName);
        $this->fillManagerFile($basicPath, $fullApplicationName);
        $this->fillLocalIni($basicPath);
    }

    private function fillDirectories(string $basicPath){
        $config = [
            'api' => [
                'controller',
                'model',
                'task',
            ],
            'config' => [],
            'config_swoole' => [],
            'data' => [
                'accessToken',
                'debug',
                'pid',
                'swooleHttpLogs',
                'swooleWebSocketLogs',
            ],
            'function' => [],
            'socket' => [
                'controller',
                'task',
            ],
            'ssl' => [],
        ];
        foreach($config as $directoryName => $item){
            $path = "{$basicPath}/{$directoryName}";
            if( false === is_dir($path)){
                mkdir($path);
                chmod($path, 0777);
            }
            foreach ($item as $value){
                $path = "{$basicPath}/{$directoryName}/{$value}";
                if( false === is_dir($path)){
                    mkdir($path);
                    chmod($path, 0777);
                }
            }
        }
    }

    private function fillPhpConfigFiles(string $basicPath){
        $content = <<<EOF
<?php
return (function(){
    return [];
})();
EOF;
        $arr = [
            'config/business.php',
            'config_swoole/httpServerConfig.php',
            'config_swoole/webSocketServerConfig.php'
        ];
        foreach ($arr as $path){
            $file = "{$basicPath}/{$path}";
            if( true === file_exists($file)){
                continue;
            }
            touch($file);
            file_put_contents($file, $content);
            chmod($file, 0777);
        }
    }

    private function fillServerIni(string $basicPath){
        $arr = [
            'config_swoole/swooleHttpServer.ini',
            'config_swoole/swooleWebSocketServer.ini',
        ];
        foreach ($arr as $path){
            $file = "{$basicPath}/{$path}";
            if( true === file_exists($file)){
                continue;
            }
            touch($file);
            copy(ROOT.'/must/config_swoole/server_demo.ini',$file);
            chmod($file, 0777);
        }
    }

    private function fillWebSocketClasses(string $basicPath){
        $content = <<<EOF
<?php
return (function () {
    \$directoryName = APP_DIRECTORY_NAME;
    return [
        'mainClass' => "\\{\$directoryName}\\socket\\controller\\MainWorkers",
        'workerClass' => "\\{\$directoryName}\\socket\\WorkersCommon",
        'managerClass' => "\\{\$directoryName}\\socket\\Manager"
    ];
})();
EOF;
        $file = "{$basicPath}/config_swoole/webSocketClasses.php";
        if( false === file_exists($file)){
            touch($file);
            file_put_contents($file, $content);
            chmod($file, 0777);
        }
    }

    private function fillMainWorkerFile(string $basicPath, string $fullApplicationName){
        $content = <<<EOF
<?php
namespace {$fullApplicationName}\socket\controller;
use Swoole\Http\Request;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;
class MainWorkers
{
    public function onOpen(Server \$ws, Request \$request){

    }
    
    public function onClose(Server \$ws, int \$fd){

    }
    
    public function onMessage(Server \$ws, Frame \$frame){

    }
    
    public function onFinish(Server \$server, int \$taskId, \$data){

    }
    
    public function onPipeMessage(Server \$server, int \$srWorkerId, \$data){
    
    }
}
EOF;
        $file = "{$basicPath}/socket/controller/MainWorkers.php";
        if( false === file_exists($file)){
            touch($file);
            file_put_contents($file, $content);
            chmod($file, 0777);
        }
    }

    private function fillWorkersFile(string $basicPath, string $fullApplicationName){
        $content = <<<EOF
<?php
namespace {$fullApplicationName}\socket;
use Swoole\WebSocket\Server;
class WorkersCommon
{
    public function onWorkerStart(Server \$server, int \$workerId){

    }
    
    public function onWorkerStop(Server \$server, int \$workerId){

    }
    
    public function onWorkerError(Server \$server,  int \$workerId, int \$workerPid, int \$exitCode, int \$signal){

    }
}
EOF;
        $file = "{$basicPath}/socket/WorkersCommon.php";
        if( false === file_exists($file)){
            touch($file);
            file_put_contents($file, $content);
            chmod($file, 0777);
        }
    }

    private function fillManagerFile(string $basicPath, string $fullApplicationName){
        $content = <<<EOF
<?php
namespace {$fullApplicationName}\socket;
use Swoole\WebSocket\Server;
class WorkersCommon
{
    public function onManagerStart(Server \$server){

    }
    
     public function onManagerStop(Server \$server){

    }
}
EOF;
        $file = "{$basicPath}/socket/Manager.php";
        if( false === file_exists($file)){
            touch($file);
            file_put_contents($file, $content);
            chmod($file, 0777);
        }
    }

    private function fillLocalIni(string $basicPath){
        $file = "{$basicPath}/config/local.ini";
        if( false === file_exists($file)){
            touch($file);
            copy(ROOT.'/must/config/demo_local.ini',$file);
            chmod($file, 0777);
        }
    }
}
(function(){
    $helper = new helper();
    $cliParams = getopt('',[
        'logs:',
    ]);
    $a = getItemFromArray($cliParams,'logs','');
    if($a !== ''){
        $helper->renameLogs($a);
        return false;
    }
    //
    $cliParams = getopt('a:',[
        'app:',
    ]);
    $a = getItemFromArray($cliParams,'a','');
    if('' === $a || false === $a){
        $a = getItemFromArray($cliParams,'app','');
    }
    if($a === ''){
        exit('没有指定参数');
    }
    $helper->run($a);
})();