<?php

use must\develop\Debugger;
use must\develop\Monitor;
use must\Launch;
use must\websocket\MainWorkers;
use Swoole\Server;

const ROOT = __DIR__;
require ROOT .'/must/Autoload.php';
require ROOT .'/must/develop/debug.php';
//
$server = (function(){
    echo true === extension_loaded('redis') ? '赞，发现redis扩展':
        '未发现redis扩展---然鹅', ',websocket通常需要redis作为辅助',PHP_EOL,PHP_EOL;
    $launch = new Launch();
    $server = $launch->initializeServer(false);
    Launch::defineApplicationConfig();
    //
    $masterObj = new \must\Master();
    $server->on('start', [ $masterObj, 'onStart']);
    $taskObj = new \must\TaskWorkers();
    $server->on('task', [ $taskObj,  'onTask']);
    //
    $directoryName = $launch->swooleConfigDirectoryName;
    $personalizedConfig = require APP_PATH.'/'.$directoryName.'/webSocketClasses.php';
    $personalizedManagerObj = new $personalizedConfig['managerClass']();
    $commonManagerObj = new \must\Manager();
    $server->on('managerStart', [ $personalizedManagerObj, 'onManagerStart']);
    $server->on('managerStop',  [ $personalizedManagerObj, 'onManagerStop']);
    $server->on('afterReload', function (Server $server) use($commonManagerObj, $personalizedManagerObj){
        $commonManagerObj->onAfterReload($server);
        $personalizedManagerObj->onAfterReload($server);
    });
    //
    $mainClass = &$personalizedConfig['mainClass'];
    $workerObj = new $personalizedConfig['workerClass']();
    $mainObj   = new MainWorkers();
    $server->on('workerStart', function(Server $server, int $workerId)
    use($mainObj, $mainClass, $workerObj){
        $mainObj->initializeController($mainClass);
        $workerObj->onWorkerStart($server, $workerId);
    });
    $server->on('workerStop',  [ $workerObj, 'onWorkerStop']);
    $server->on('workerError', [ $workerObj, 'onWorkerError']);
    //
    $server->on('open',    [ $mainObj, 'onOpen']);
    $server->on('message', [ $mainObj, 'onMessage']);
    $server->on('close',   [ $mainObj, 'onClose']);
    $server->on('finish',  [ $mainObj, 'onFinish']);
    $server->on('pipeMessage', [ $mainObj, 'onPipeMessage']);
    //
    return $server;
})();
//热更新
Monitor::watchByProcess([
    Launch::APP_COMMON_PATH,
    APP_PATH .'/socket/controller',
    APP_PATH .'/socket/task',
    APP_PATH.'/config',
    APP_PATH.'/function',
]);
//启动
echo Debugger::cliSetColor('启动 Swoole WebSocket server...','green').PHP_EOL.PHP_EOL;
$server->start();