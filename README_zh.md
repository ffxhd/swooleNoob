
#swoole
* 不要用$_*变量，否则立即报错；

* 需要让业务代码停下来，禁止exit/die, 请
<blockquote>
throw new \Exception('中断业务代码的执行')
</blockquote>

# debug
强烈推荐:
<blockquote>
<pre>
say($variable);
say('info',$info,'where',$where,'pageSize',$pageSize);
</pre>
</blockquote>
say()的参数的个数无限的，就像 javaScript 的 console.log();
say()会告诉你，你在哪边调用了它。
如果在linux 终端下,输出是五彩缤纷的。

![alt effect](./onRequest/public/images/php-cli-colorful2.png "effect")

# 要求:
* php 7.4
* 为了热更新，本地开发需要php的inotify扩展; 建议安装文本转语音的软件：apt-get install espeak，测试：
espeak "php is best"
* 为了会话，本地和远程服务器需要安装redis，需要php的redis扩展。
* 为了本地开发时，能够在windows编辑linux虚拟机中的文件，请在virtualbox配置仅主机（host-only）网卡(不连接wifi也能本地开发)，
安装samba服务, 然后sudo service smbd start