<?php
namespace must;
use app_common\core\session\SessionFactory;
use app_common\DB;
use must\develop\Debugger;
use Swoole\Server;

class WorkersCommon
{
    public ?int $workerId = null;
    public ?bool $isTaskWorker = null;

    /**
     * @param $server
     * @param int $worker_id
     * @param int $worker_pid
     * @param int $exit_code
     * @param int $signal
     * @return bool
     */
    public function onWorkerError($server,  int $worker_id, int $worker_pid, int $exit_code, int $signal)
    {
        /*signal = 11：说明Worker进程发生了segment fault段错误，可能触发了底层的BUG，
        请收集core dump信息和valgrind内存检测日志，向我们反馈此问题
        exit_code = 255：说明Worker进程发生了Fatal Error致命错误，请检查PHP的错误日志，
        找到存在问题的PHP代码，进行解决
        signal = 9：说明Worker被系统强行Kill，请检查是否有人为的kill -9操作，
        检查dmesg信息中是否存在OOM（Out of memory）
        如果存在OOM，分配了过大的内存。检查Server的setting配置，
        是否创建了非常大的Swoole\Table、Swoole\Buffer等内存模块*/
        $myRole = $this->describeMyRole($server->taskworker);
        $trace = <<<EOF
woker出现错误: worker_id={$worker_id},  worker_pid={$worker_pid},
exit_code={$exit_code},  signal={$signal}
EOF;
        if( true === IS_LOCAL)
        {
            say('woker出现错误--', $trace);
            exec("espeak 'worker error'");

            //开始
            $redis = connectRedisServer();
            $time = time();
            $formatTime = date('Y-m-d H:i:s',$time);
            $redis->hSet("{$myRole}_{$worker_id}",'worker_error', "worker_pid={$worker_pid},
exit_code={$exit_code},  signal={$signal}, time={$formatTime}");
            //结束
        }
        else
        {
            if( $signal === 9)
            {
                return true;
            }
            $title = "远程swoole的{$myRole}出问题啦";
            $dateTime = date('Y-m-d H:i:s');
            $body = "出错时间：{$dateTime}<br/>{$trace}";
            Debugger::reportErrorBySendMail($title,$body);
        }
        return true;
    }

    private function describeMyRole($isTaskWorker): string
    {
        return true === $isTaskWorker ? 'taskWorker' : 'mainWorker';
    }

    /**如果想使用Reload机制实现代码重载入，必须在onWorkerStart中require你的业务文件，
    而不是在文件头部。
    在onWorkerStart调用之前已包含的文件，不会重新载入代码。
     * 可以将公用的、不易变的php文件放置到onWorkerStart之前。
    这样虽然不能重载入代码，但所有Worker是共享的，不需要额外的内存来保存这些数据。
    onWorkerStart之后的代码每个进程都需要在内存中保存一份
     *
     * 是否可以共用1个redis或mysql连接
    绝对不可以。必须每个进程单独创建Redis、MySQL、PDO连接，其他的存储客户端同样也是如此。
    原因是如果共用1个连接，那么返回的结果无法保证被哪个进程处理。
    持有连接的进程理论上都可以对这个连接进行读写，这样数据就发生错乱了。
     * 在swoole_server中，应当在onWorkerStart中创建连接对象
     * @param $server
     * @param $worker_id
     */
    public function onWorkerStart(Server $server, $worker_id): void
    {
        //覆盖配置，此处是为了修改业务配置后，代码热重载
        \must\Launch::defineApplicationConfig();

        //自动加载各种库
        $file = ROOT . '/vendor/autoload.php';
        if( true === file_exists($file))
        {
            require $file;
        }
        unset($file);

        //引入各种自定义函数
        Launch::requireFunctions(\must\Launch::APP_COMMON_PATH . '/function');
        Launch::requireFunctions(APP_PATH . '/function');
        //记住配置，以便需要时连接数据库，比如:MySQL
        DB::setConfig(APP_CONFIG['db_config']);
        //
        $this->isTaskWorker = $isTaskWorker = $server->taskworker;
        $this->workerId = $worker_id;
        $myRole = $this->describeMyRole($isTaskWorker);
        new SessionFactory();
        //
        $isCli = Debugger::isOutputForTerminal();
        $str = Debugger::sayByCase( $this->workerId,'onWorkerStart--workerId=', $isCli, true);
        $str = trim($str,PHP_EOL);
        $str2 = Debugger::sayByCase($server->worker_pid,'pid=', $isCli, true);
        $str2 = trim($str2,PHP_EOL);
        echo $str.$str2."我是{$myRole}".PHP_EOL;
        //
        if(true === IS_LOCAL){
            //开始
            $time = time();
            $formatTime = date('Y-m-d H:i:s',$time);
            $redis = connectRedisServer();
            $redis->hMSet("{$myRole}_{$worker_id}",[
                'worker_id' => $worker_id,
                'pid' => $server->worker_pid,
                'start_unix_time' => $time,
                'start_format_time' => $formatTime
            ]);
            //结束
        }
    }

    public function onWorkerStop(Server $server, int $worker_id){
        //开始
        $redis = connectRedisServer();
        $myRole = $this->describeMyRole($server->taskworker);
        $time = time();
        $formatTime = date('Y-m-d H:i:s',$time);
        $redis->hSet("{$myRole}_{$worker_id}",'stop_format_time', $formatTime);
        //结束
    }
}