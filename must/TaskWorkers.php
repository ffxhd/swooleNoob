<?php
namespace must;

use app_common\controller\DbBusiness;
use app_common\DB;
use Exception;
use must\develop\Debugger;
use Swoole\Http\Server;
use Swoole\Server\Task;

class TaskWorkers extends WorkersCommon
{
    /*$task_id是任务ID，由swoole扩展内自动生成，用于区分不同的任务。$task_id和$src_worker_id组合起来才是全局唯一的，
    不同的worker进程投递的任务ID可能会有相同
    $src_worker_id来自于哪个worker进程
    $data 是任务的内容*/
    /**
     *
     * @param Server $server
     * @param Task $task
     * @return array
     * @throws Exception
     */
    public function onTask(Server $server, Task $task): array//协程版
    {
        $fromMainWorkerId = $task->worker_id;//来自哪个`Worker`进程
        $taskId = $task->id;//任务的编号
        $taskType = $task->flags;// //任务的类型，taskwait, task, taskCo, taskWaitMulti 可能使用不同的 flags
        $taskData = $task->data;//任务的数据
        /*if( true === IS_LOCAL)
        {
            say('协程版onTask--data',$taskData,
                'onTask--$workerId',$taskWorkerId,
                'onTask--$taskId',$taskId
            );
        }*/
        $purpose = getItemFromArray($taskData,'purpose','');
        switch ($purpose)
        {
            case 'reportErrorToDevelopers':
                $contentToReport = getItemFromArray($taskData,'content','');
                Debugger::reportErrorBySendMail('php程序出错啦',$contentToReport);
                $task->finish('通过邮件报告开发者程序出错成功');
                break;
            case 'business':
                say('收到 业务型任务',$taskData);
                $class = getItemFromArray($taskData,'class','');
                $method = getItemFromArray($taskData,'method','');
                if($class === '' || $method === ''){
                    $task->finish('没有指定class或者method');
                }
                DbBusiness::initializeInstance();
                $instance = new $class();
                $instance->$method($taskData['data']);
                DB::stmtError();
                DbBusiness::deleteInstance();
                break;
            default:
        }
        $task->finish([]);
        return [];
    }
}