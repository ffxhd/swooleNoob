<?php
return [
    'task_use_object' => true,
    'enable_coroutine' => true,//enable_coroutine参数，默认为true，通过设置为false可关闭内置协程。
    'task_enable_coroutine' => true,//task_enable_coroutine必须在enable_coroutine == true时才可以使用
];
