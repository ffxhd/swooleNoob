<?php

namespace must;


use Swoole\Server;

class Master
{
    public function onStart(Server $server){
        //记录pid, 便于restart
        $file = getItemFromArray($server->setting,'pid_file');
        if($file === ''){
            return false;
        }
        file_put_contents($file,$server->master_pid);
        return true;
    }
}