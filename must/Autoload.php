<?php
namespace must;
class Autoload
{
    private static array $classFiles = [];
	public static function load($className): bool
    {
        if( true === isset( self::$classFiles[$className]))
        {
            return  false;
        }
        $filePartPath = sprintf('%s.php',str_replace('\\', '/', $className));
        $filePath = ROOT.'/'.$filePartPath;
        if( false === file_exists($filePath))
        {
            return false;
        }
        require_once  $filePath;
        self::$classFiles[$className] = $filePath;
        return true;
	}
}
spl_autoload_register(array('must\\Autoload','load'));

