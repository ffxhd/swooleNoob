<?php
use must\develop\Debugger;

/**
 * @param array $arr
 * @param string $field
 * @param mixed $default
 * @return mixed
 */
function getItemFromArray(array $arr, string $field, mixed $default=''): mixed
{
    return true === isset( $arr[$field] ) ? $arr[$field] : $default;
}

//say2('mean1',$var,'mean2',$var2); 0-2-4-6为解释
function say(...$manyParams)
{
    $isCLI = Debugger::isOutputForTerminal();
    $br = true === $isCLI ? PHP_EOL :'<br/>';
    //回溯，不然记不得哪个位置调用了这个方法，debug完成后不知道上哪儿注释。
    if( function_exists('xdebug_call_function') )
    {
        //xdebug_call_*  系列的函数必须发放在这里，才能知道哪里调用了say方法
        $callSayClass = xdebug_call_class();
        $callSayFunc =  xdebug_call_function();
        $callSayFile =  xdebug_call_file();
        $callSayLine = xdebug_call_line();
    }
    else
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,1);
        $trace = $trace[0];
        /*echo '$traceInfo---<pre>';
        print_r($traceInfo);
        echo '</pre>';*/
        $callSayClass = getItemFromArray($trace,'class');
        $callSayFunc =  getItemFromArray($trace,'function');
        $callSayFile =  getItemFromArray($trace,'file');
        $callSayLine =  getItemFromArray($trace,'line');
        unset($trace);
    }
    $callSayClass = $callSayClass ? $callSayClass.'的 ' : '';
    $stack = "{$callSayClass}{$callSayFunc}()，在 {$callSayFile} 的第 {$callSayLine} 行";
    $stackStr =  true === $isCLI ? "{$stack}{$br}": <<<EOF
    <span style="color:gray">{$stack}</span><br/>
EOF;
    //
    $allContents = '';
    $isStrict = true;//true === $isStrict || 1 === $isStrict;
    $str = '';
    if( count($manyParams) === 1)
    {
        $allContents = Debugger::sayByCase($manyParams[0], $str,$isCLI,$isStrict);
    }
    else
    {
        //say2('mean1',$var,'mean2',$var2); 0-2-4-6为解释
        //say2($var,'$mean1'$var2,,'$mean2',); 1-3-5-7为解释
        foreach($manyParams as $seq => $arg )
        {
            if( $seq % 2 === 0 )
            {
                $str = $arg;
            }
            else
            {
                $allContents .=  Debugger::sayByCase($arg, $str,$isCLI,$isStrict);
            }
        }
    }
    //
    //style="font-size:52px"
    $el_start = true === $isCLI ? '' : '<div >';
    $el_end   = true === $isCLI ? '' : '</div>';
    $output = <<<EOF
        {$el_start}{$allContents}{$stackStr}{$el_end}
EOF;
    echo $output;
}

function strictDump($mix, $str,$isStrict=true)
{
    $isCLI = Debugger::isRunInCLI();
    $style = $isCLI === true ? '' : <<<EOF
    font-size:18px;
white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word;       /* Internet Explorer 5.5+ */
EOF;
    $arrStyle_start = true === $isCLI ? "{$str}=>" : "<div style=\"{$style}\"><mark>{$str}</mark>=>";
    $arrStyle_end = true === $isCLI ? '' : '</div></br/>';
    echo $arrStyle_start;
    if( true === $isStrict)
    {
        var_dump($mix);
    }
    else
    {
        print_r($mix);
    }
    echo $arrStyle_end;
}

function connectRedisServer():\Redis
{
    $config = APP_CONFIG;
    $redis = new \Redis();
    $redisConfig = $config['redis'];
    $redis->connect($redisConfig['host'],$redisConfig['port']);
    $redis->auth($redisConfig['password']);
    $redis->select($config['debug']['redis']['dbIndex']);
    return $redis;
}
