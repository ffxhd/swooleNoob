<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/3/26
 * Time: 11:11
 */

/**
 * @param string $cli_file  绝对路径
 * @param array|string  $cli_paramMix  数组[a=>b,a=>b],值不要有空格
 */
function runInCli($cli_file, $cli_paramMix)
{
    $exePath = '/usr/bin/php';
    $cli_params = true === is_array( $cli_paramMix ) ? composeExecParams($cli_paramMix) : $cli_paramMix;
    exec("{$exePath}  {$cli_file} {$cli_params}");//必须 有空格
}

/*例如：$cliPramsConfig = [
    'port' =>[
        'mean' => 'port',
        'idealType' =>'number',
        'require'=>true,
        'mustBeInteger'=> true,
    ],
    'host' =>[
        'mean' => 'host',
        'idealType' =>'string',
        'require'=>true,
    ],
    'ctrl' =>[
        'mean' => 'ctrl',
        'idealType' =>'string',
        'require'=>true,
    ],
];
方便验证参数值
    */
function cli_getParams($paramsArr)
{
    $longOptArr = array();
    foreach($paramsArr as $field => $item )
    {
        $longOptArr[] = $field.':';
    }
    return getopt('', $longOptArr);
}

function composeExecParams($cli_paramArr)
{
    $cli_params = '';
    foreach($cli_paramArr as $cli_param => $value )
    {
        $cli_params.="  --{$cli_param}  '{$value}'  ";
    }
    return $cli_params;
}

function getLocalIp()
{
    $content = shell_exec('ifconfig  enp0s8 | grep inet');
    $arr = explode('netmask',$content);
    $serverBindHost = $arr[0];
    $serverBindHost = trim($serverBindHost);
    $serverBindHost = trim($serverBindHost,'inet');
    unset($content,$arr);
    return trim($serverBindHost);
}


