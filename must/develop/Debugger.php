<?php

namespace must\develop;
use JetBrains\PhpStorm\Pure;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Swoole\Server;

//变量类型不同，打印方法不同： array、object + [ string,  integer/double， null， bool ]
//CGI，简洁打印
//cli下，不要html标签
use JetBrains\PhpStorm\ArrayShape;

class Debugger
{
// public static $isInCLI = false;
    //font-size:54px;
    private const foregroundColors = [
        'black' => '0;30',
        'dark_gray' => '1;30',
        'red' => '0;31',
        'light_red' => '1;31',
        'green' => '0;32',
        'light_green' => '1;32',
        'brown' => '0;33',//棕色
        'yellow' => '1;33',
        'blue' => '0;34',
        'light_blue' => '1;34',
        'purple' => '0;35',
        'light_purple' => '1;35',
        'cyan' => '0;36',//青色
        'light_cyan' => '1;36',
        'light_gray' => '0;37',
        'white' => '1;37',
    ];
    private const backgroundColors = [
        'black' => '40',
        'red' => '41',
        'green' => '42',
        'yellow' => '43',
        'blue' => '44',
        'magenta' => '45',//品红
        'cyan' => '46',
        'light_gray' => '47',
    ];

    //=====================================================================================================

    public static function cliSetColor($content, string $color): string
    {
        $value = self::foregroundColors[$color];
        return "\e[{$value}m{$content}\e[0m ";
    }

    public static function cliSetBackgroundColor($content, string $color): string
    {
        $value = self::backgroundColors[$color];
        return "\e[{$value}m{$content}\e[0m ";
    }

    private static function  getTypeStr($variableType, bool $isCLI, bool $isJustClean): string
    {
        if( true === $isCLI )
        {
            return  "[{$variableType}]";
        }
        return  true === $isJustClean ? '' :
            "<span style=\"color:#D3D3D3;margin-right: 10px;\">{$variableType}</span>";
    }

    public static function  sayByCase($mix, $str, bool $isCLI, bool $isStrict): string
    {
        $variableType = gettype($mix);
        if( true === $isCLI)
        {
            $str = self::cliSetColor($str,'yellow');
        }
        $isJustClean =  $str === ''|| $str === null;
        $arrow = true === $isJustClean ? '' : '=>';
        $typeStr = self::getTypeStr($variableType, $isCLI, $isJustClean);
        $isNeedSayBr = true;
        switch ($variableType)
        {
            case 'string':
                $idealString = self::getBodyWhenIsString($mix, $isCLI);
                if( true === $isCLI )
                {
                    $typeStr = true === $isJustClean ? '' : $typeStr;
                    $typeStr = self::cliSetColor("$typeStr",'cyan');
                }
                break;
            case 'integer':
            case 'double':
                $idealString =  self::getBodyWhenIsNumber($mix, $isCLI);
                if( true === $isCLI )
                {
                    $typeStr = self::cliSetColor("$typeStr",'cyan');
                }
                break;
            case 'boolean':
                $idealString =  self::getBodyWhenIsBool($mix, $isCLI);
                if( true === $isCLI )
                {
                    $typeStr = self::cliSetColor("$typeStr",'cyan');
                }
                break;
            case 'NULL'://必须 全部大写
                if( true === $isCLI )
                {
                    $typeStr = self::cliSetColor("$typeStr",'cyan');
                }
                $idealString =  self::getBodyWhenIsNull($isCLI);
                break;
            case 'array':
            case 'object':
            case 'resource':
                $isNeedSayBr = false;
                $idealString =  self::getBodyWhenIsArr($mix, $isCLI, $isStrict);
                if( true === $isCLI )
                {
                    $typeStr = self::cliSetColor("$typeStr",'cyan');
                }
                break;
            default://unknown type
                $idealString =  "[$variableType]";
                if( true === $isCLI )
                {
                    $typeStr = self::cliSetColor("$typeStr",'cyan');
                }
        }
        if( true === $isNeedSayBr )
        {
            $idealString .=  false === $isCLI ?'<br/>' :PHP_EOL;
        }
        return "{$str}{$arrow}{$typeStr}{$idealString}";
    }

    private static function getBodyWhenIsString($mix, bool $isCLI): string
    {
        $isEmpty =  $mix === '';
        if( true === $isCLI )
        {
            $content = true === $isEmpty ? '[空字符串]' : $mix;
            return  self::cliSetColor($content,'brown');
        }
        return  true === $isEmpty ?  "<span style=\"color:#D3D3D3\">空字符串</span>" :
            "<span>{$mix}</span>";
    }

    private static function getBodyWhenIsNumber($mix, bool $isCLI): string
    {
        if( true === $isCLI )
        {
            return  self::cliSetColor($mix,'light_blue');
        }
        return   "<span style=\"color:#5B75FF\">{$mix}</span>";
    }

    private static function getBodyWhenIsBool($mix, bool $isCLI): string
    {
        $boolMean = $mix === true ? 'true' : 'false';
        if( true === $isCLI )
        {
            $color = $mix === true ? 'light_green' : 'light_red';
            return self::cliSetColor("[{$boolMean}]",$color);
        }
        $color = $mix === true ? 'forestgreen' : 'blue';
        return  "<span style=\"color:{$color}\">{$boolMean}</span>" ;
    }

    private static function getBodyWhenIsNull(bool $isCLI): string
    {
        if( true === $isCLI )
        {
            return self::cliSetColor("[null]",'light_cyan');
        }
        return   "<span style=\"color:#D3D3D3\">null</span><br/>";
    }

    private static function getBodyWhenIsArr($mix, bool $isCLI, bool $isStrict): bool|string|null
    {
        $body = true === $isStrict? var_export($mix,true) : print_r($mix,true);
        if( true === $isCLI )
        {
            return  $body;
            //return  self::cliSetBackgroundColor($body,'green');
        }
        $style = <<<EOF
        font-size: 18px;
white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word;       /* Internet Explorer 5.5+ */
EOF;
        return  "<pre style=\"{$style}\">".$body.'</pre>';
    }

    //=====================================================================================================

    public static function isOutputForTerminal(): bool
    {
        $isCLi = self::isRunInCLI();
        if(false === $isCLi){
            return false;
        }
        $obInfo = ob_get_status();
        //有内容的数据=>ob开启=>浏览器
        if(false === empty($obInfo)){
            return false;
        }
        /*
        空数组=>ob未开启=>文件(守护模式)
        空数组=>ob未开启=>终端(其他模式)
         */
        global $server;
        if($server instanceof Server){
            return getItemFromArray($server->setting,'daemonize', false) === false;
        }
        return true;
    }

    /**
     * 判断当前的运行环境是否是cli模式
     * @return bool
     */
    public static function isRunInCLI(): bool
    {
        static $isCLi = null;
        if( null === $isCLi)
        {
            $type  = php_sapi_name();
            $isCLi = preg_match("/cli/i",$type);
            $isCLi = boolval($isCLi);
        }
        return $isCLi;
    }

    /**
     * 根据swoole获取到的本地ip列表, 判断是否是本地开发
     * @return bool
     */
    public static function assertLocalByIpList(): bool
    {
        $ipList = swoole_get_local_ip();
        foreach($ipList as $ip)
        {
            if( strpos($ip,'192.168.') === 0) {
                return true;
            }
        }
        return false;
    }

    //=====================================================================================================

    #[ArrayShape([
        'errNotify' => "string",
        'trace' => "string"
    ])]
    public static function getPHPError($error_level, $error_message, $error_file, $error_line, array $traceList = []): array
    {
        $error_level = getItemFromArray(APP_CONFIG['errorLevelExplain'], $error_level, $error_level);
        $errNotify = "{$error_level}：{$error_message}";
        $traceList = false === empty($traceList) ? $traceList : debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $trace = self::traceHTML($traceList,'html',$errNotify,$error_level,$error_file,$error_line);
        return [
            'errNotify' => $errNotify,
            'trace' => $trace
        ];
    }

    private static function traceHTML($traceList, $way, $errNotify, $error_level, $error_file, $error_line): string
    {
        $wrap2 = PHP_EOL;
        $errMsg =  "{$errNotify} 在 {$error_file} 第{$error_line}行";
        $traceList = array_reverse($traceList);
        $isAlert = $way ==='alert';
        $trace = true === $isAlert ? '' : <<<EOF
    <p style="font-size:24px;color:black;background-color:yellow;">{$errMsg}</p>
    <ul style="list-style-type: none">{$wrap2}
EOF;//orange
        $wrap = true === $isAlert ? "\n" : '<br/>';
        foreach($traceList as $k => $traceArr )
        {
            $traceClass = getItemFromArray($traceArr,'class');
            $traceType = getItemFromArray($traceArr,'type');
            $traceFunction = getItemFromArray($traceArr,'function');
            $traceFile = getItemFromArray($traceArr,'file');
            $traceLine = getItemFromArray($traceArr,'line');
            $args = getItemFromArray($traceArr,'args',[]);
            if( true === empty($args))
            {
                $argHtml = '';
            }
            else
            {
                $args = print_r($args, true);
                $argHtml =  "参数：<div><pre>{$args}</pre></div>";
            }
            if( true === $isAlert )
            {
                $trace.=<<<EOF
            方法：{$traceClass} {$traceType}{$traceFunction}(){$wrap}
            文件：{$traceFile}第：{$traceLine}行{$wrap}
EOF;
            }
            else
            {
                //{$traceArr['object']}
                /*style="background-color:lawngreen;"*/
                if( $traceClass && stripos($traceClass,'controller')!==false )
                {
                    $highLight =  'color:#8A2BE2;';//#f57900
                }
                else if($traceLine === $error_line && $error_level === 'Warning')
                {
                    $highLight =  'color:orangeRed;';
                }
                else{
                    $highLight = '';
                }
                //say($light_style,$traceClass.'--$light_style');
                $hr = $k > 0 ? '<hr/>':'';
                $trace .=<<<EOF
        <li>
            {$hr}
            <p style="color:gray">{$traceFile}  第{$traceLine}行</p>
            <p style="{$highLight}">{$traceClass}{$traceType}{$traceFunction}()</p>
            {$argHtml}
        <li/>{$wrap2}
EOF;
            }
        }
        if( false === $isAlert )
        {
            $trace .= "    </ul><hr/>{$wrap2}";
        }
        return $trace;
    }

    public static function getAllPHPErrorAsString(array &$list): string
    {
        return implode('',$list);
    }

    public static function reportErrorBySendMail($title, $body): void
    {
        $mailConfig = APP_CONFIG['remoteBug_sendMail'];
        say('准备发送邮件',$mailConfig);
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            //服务器配置
            $mail->setLanguage('zh_cn');
            $mail->CharSet ="UTF-8"; //设定邮件编码
            $mail->SMTPDebug = \PHPMailer\PHPMailer\SMTP::DEBUG_CONNECTION; // 调试模式输出
            $mail->isSMTP();// 使用SMTP
            $mail->Host = $mailConfig['host'];// SMTP服务器
            $mail->SMTPAuth = true;// 允许 SMTP 认证
            $mail->Username = $mailConfig['e-mail']; // SMTP 用户名  即邮箱的用户名
            $mail->Password = $mailConfig['auth'];// SMTP 密码  部分邮箱是授权码(例如163邮箱)
            $mail->SMTPSecure = 'ssl';// 允许 TLS 或者ssl协议
            $mail->Port = $mailConfig['port'];  //ssl   // 服务器端口 25 或者465 具体要看邮箱服务器支持
            $mail->setFrom( $mail->Username , 'Mailer');  //发件人
            $mail->addAddress( $mail->Username , 'Joe');  // 收件人
            //$mail->addAddress('ellen@example.com');  // 可添加多个收件人
            //$mail->addReplyTo('xxxx@163.com', 'info'); //回复的时候回复给哪个邮箱 建议和发件人一致
            //$mail->addCC('cc@example.com');                    //抄送
            //$mail->addBCC('bcc@example.com');                    //密送

            //发送附件
            // $mail->addAttachment('../xy.zip');         // 添加附件
            // $mail->addAttachment('../thumb-1.jpg', 'new.jpg');    // 发送附件并且重命名

            //Content
            $mail->isHTML(true);                                  // 是否以HTML文档格式发送  发送后客户端可直接显示对应HTML内容
            $mail->Subject = $title;//这里是邮件标题
            $mail->Body    = $body ;//这里是邮件内容
            $mail->AltBody = '如果邮件客户端不支持HTML则显示此内容';

            $mail->send();
            echo '邮件发送成功';
        }
        catch (Exception $e)
        {
            self::putPhpErrorContentToFile($body);
            echo '邮件发送失败: ', $mail->ErrorInfo;
        }
    }

    public static function putPhpErrorContentToFile(string $contentToReport): void
    {
        $content = date('Y-m-d H:i:s').PHP_EOL.
            '$server->task() fail, so put php error in file : ' .PHP_EOL.
            $contentToReport.PHP_EOL.PHP_EOL;
        file_put_contents(APP_PATH.'/data/debug/php_error.html', $content);
    }

    public static function  jsonDecodeError():string
    {
        $error = json_last_error();
        $errMsgArr = [
            JSON_ERROR_NONE => '没有错误发生',
            JSON_ERROR_DEPTH =>'到达了最大堆栈深度',
            JSON_ERROR_STATE_MISMATCH =>'无效或异常的 JSON',
            JSON_ERROR_CTRL_CHAR =>'控制字符错误，可能是编码不对',
            JSON_ERROR_SYNTAX =>'语法错误',
            JSON_ERROR_UTF8 =>'异常的 UTF-8 字符，也许是因为不正确的编码。',
            JSON_ERROR_RECURSION =>'One or more recursive references in the value to be encoded',
            JSON_ERROR_INF_OR_NAN =>'One or more NANor INFvalues in the value to be encoded',
            JSON_ERROR_UNSUPPORTED_TYPE =>'指定的类型，值无法编码。',
            JSON_ERROR_INVALID_PROPERTY_NAME =>'指定的属性名无法编码。',
            JSON_ERROR_UTF16 =>'畸形的 UTF-16 字符，可能因为字符编码不正确。',
        ];
        return getItemFromArray($errMsgArr, $error, '未知错误');
    }
}