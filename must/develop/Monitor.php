<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/19
 * Time: 20:18
 */
namespace must\develop;
use Swoole\Event;
use Swoole\Process;

class Monitor
{
    public     $notify;
    private array $directories = [];
    public array  $allItems = [];

    /**
     * Monitor constructor.
     * @param array $directories
     */
    public function  __construct(array $directories)
    {
        $this->notify = inotify_init();
        $this->directories = $directories;
        $this->init();
    }

    protected function init()
    {
        $this->watchMainDir();
        foreach($this->directories as $item)
        {
            $this->recursiveWatch($item);
        }
    }

    /**
     * 2019.07.28 由监听单个文件夹，改为监听多个文件夹
     */
    public function watchMainDir()
    {
        foreach($this->directories as $directory)
        {
            //主目录打上监听器
            $this->allItems[]= [ $directory => '主目录' ];
            $this->addWatch_dir($directory);
        }
    }

    protected  function addWatch_dir($item)
    {
        inotify_add_watch($this->notify, $item, IN_CREATE | IN_DELETE | IN_MODIFY );
    }

    /**
     * 递归监听指定文件夹
     * @param string $dirToListen
     * @return bool
     */
    public function recursiveWatch(string $dirToListen): bool
    {
        //主目录的文件、子目录及其文件打上监听器
        $arr = scandir($dirToListen);
        if(false === is_array($arr))
        {
            return false;
        }
        foreach($arr as $str )
        {
            if( $str === '.'|| $str === '..')
            {
                continue;
            }
            $item = $dirToListen.'/'.$str;
            if( true === is_file($item) )
            {
                $this->allItems[]= [ $item => '文件'];
                $this->addWatch_dir($item);
            }
            else if( true === is_dir($item) )
            {
                $this->allItems[]= [ $item => '目录' ];
                $this->addWatch_dir($item);
                $this->recursiveWatch($item);
            }
        }
        return true;
    }

    public function watchSthHappen($reloadFunc): bool
    {
        $events = inotify_read($this->notify);
        //say('watchSthHappen $events',$events);
        if (true === empty($events))
        {
            return false;
        }
        //如果新增文件夹或者文件，则对其打上监听器=>重头开始监听
        $isStartOver = false;
        foreach($events as $event)
        {
            if(  1073742080 === $event['mask'])
            {
                $isStartOver = true;
                break;
            }
        }
        call_user_func($reloadFunc);
        if($isStartOver === true )
        {
            $this->allItems = [];
            $this->init();
            echo '重头开始监听'.PHP_EOL;
            //var_dump($allItems);
        }
        return true;
    }

    public static function watchByProcess(array $directories){
        global $server;
        $process = new Process(function(Process $process)
        use($server,$directories)
        {
            say('监听之前：$directories',$directories);
            $monitor = new Monitor($directories);
            Event::add($monitor->notify, function () use($monitor,$server){
                //say("swoole event add--{$worker_id}");
                $monitor->watchSthHappen(function() use($server)
                {
                    $server->reload();//Base模式不支持reload Task进程
                    return true;
                });
            });
            Event::wait();//启动事件监听
        });
        $process->name('phpListenFilesChange');
        $server->addProcess($process);
    }
}
