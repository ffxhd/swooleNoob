<?php
namespace must;
use Swoole\Coroutine\System;
use Swoole\Server;
use function Swoole\Coroutine\run;

class Manager
{
    public function onManagerStart(Server $server){

    }


    public function onManagerStop(Server $server){

    }

    public function onAfterReload(Server $server): bool
    {
        $dateTime = date('Y-m-d H:i:s');
        $host = $server->host;
        $port = $server->port;
        $schemeKind = $server instanceof \Swoole\WebSocket\Server ? 'websocket' : 'http';
        echo "{$schemeKind}-server://{$host}:{$port}已经重新载入代码--{$dateTime}".PHP_EOL;
        if(false === IS_LOCAL){
            return false;
        }
        run(function($schemeKind){
            $file = APP_PATH.'/data/debug/reload.txt';
            System::exec("espeak '{$schemeKind} server have reload codes' > {$file}");
        }, $schemeKind);
        return true;
    }
}