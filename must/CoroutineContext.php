<?php

namespace must;

use Swoole\Coroutine;

class CoroutineContext
{
    private static array $pool = [];

    private static function getCoroutineId() : int
    {
        return Coroutine::getCid();
    }

    private static function isExistsThisCoroutine(int $coroutineId): bool
    {
        return Coroutine::exists($coroutineId);
    }

    public static function get(string|int $key){
        $cid = self::getCoroutineId();
        if(false === self::isExistsThisCoroutine($cid)) {
            return null;
        }
        if(true === isset(self::$pool[$cid][$key])){
            return self::$pool[$cid][$key];
        }
        return null;
    }

    public static function put(string|int $key, $item): void
    {
        $cid = self::getCoroutineId();
        if(true === self::isExistsThisCoroutine($cid))
        {
            self::$pool[$cid][$key] = $item;
        }
    }

    public static function delete(string|int|null $key): void
    {
        $cid = self::getCoroutineId();
        if(true === self::isExistsThisCoroutine($cid))
        {
            if($key === null){
                unset(self::$pool[$cid]);
            }
            else{
                unset(self::$pool[$cid][$key]);
            }
        }
    }
}
