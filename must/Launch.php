<?php
namespace must;
use must\develop\Debugger;
use Swoole\Http\Server as SwooleHttpServer;
use Swoole\WebSocket\Server as SwooleWebSocketServer;
class Launch{
    const APP_COMMON_PATH = ROOT.'/app_common';
    public string $swooleConfigDirectoryName = 'config_swoole';

    public function __construct(){
        date_default_timezone_set('PRC');//PRC，中华人民共和国，People's Republic of China
        define('IS_LOCAL', Debugger::assertLocalByIpList());
        if( true === extension_loaded('inotify') ) {
            echo  '赞，发现发现inotify扩展, 可以自动热加载业务目录中的文件了' .PHP_EOL;
        }
        else{
            $str = '未发现inotify扩展( 安装及使用详见 http://php.net/inotify )---';
            $str.= true === IS_LOCAL ? '然鹅，本地开发，为了提升开发体验，需要自动热加载业务目录中的文件，'
                 : '为了云服务器在git pull之后能够自动热加载业务目录中的文件，'.
                '避免重启整个Swoole Server造成服务中断（比如WebSocket业务、MySQL事务）, ';
            $str.= '需要利用此扩展监听这些目录'.PHP_EOL.PHP_EOL;
            echo $str;
            unset($str);
        }
    }

    /**
     * 注意 只能使用一次
     * @param bool $isHttpServer
     * @return SwooleHttpServer|SwooleWebSocketServer
     */
    public function initializeServer(bool $isHttpServer): SwooleHttpServer|SwooleWebSocketServer
    {
        $serverConfig = $this->chooseOneSite($isHttpServer);
        //
        $host = &$serverConfig['host'];
        $port = &$serverConfig['port'];
        $swooleConfig = &$serverConfig['swooleConfig'];
        $swooleFinalConfig = $this->buildHttpServerConfig($isHttpServer, $swooleConfig, $host);
        $this->kill($swooleFinalConfig['pid_file']);
        if(true === $isHttpServer && true === IS_LOCAL){
            $this->flushRedisDB();
        }
        //
        $isUseSSL = true === isset($swooleFinalConfig['ssl_cert_file']) && true === isset($swooleFinalConfig['ssl_key_file']);
        //
        $scheme = true === $isHttpServer ? 'http' : 'ws';
        if(true === $isUseSSL){
            $scheme.= 's';
        }
        say("{$scheme}://{$host}:{$port}--swoole-server--最终配置", $swooleFinalConfig);
        $text = Debugger::cliSetColor("绑定的域名、端口：{$host}:{$port}，".
            " 访问：{$scheme}://{$host}:{$port}",'green');
        echo $text.PHP_EOL;
        unset($scheme, $text);
        //
        $sockType = true === $isUseSSL ? SWOOLE_SOCK_TCP | SWOOLE_SSL : SWOOLE_SOCK_TCP;
        if(true === $isHttpServer){
            $server = new SwooleHttpServer($host, $port,SWOOLE_PROCESS, $sockType);
        }
        else{
            $server = new SwooleWebSocketServer($host, $port,SWOOLE_PROCESS, $sockType);
        }
        $server->set($swooleFinalConfig);
        return $server;
    }

    /**
     * 注意 只能使用一次
     * @param bool $isHttpServer
     * @return array
     */
    public function chooseOneSite(bool $isHttpServer): array
    {
        $iniFileName = true === $isHttpServer ? 'swooleHttpServer.ini' : 'swooleWebSocketServer.ini';
        $directories = scandir(ROOT);
        $prefix = 'app_';
        $hosts = [];
        $menuArray = [];
        $configDirectoryName = $this->swooleConfigDirectoryName;
        foreach ($directories as &$value) {
            if ($value === '.' || $value === '..') {
                continue;
            }
            if (strpos($value, $prefix) !== 0) {
                continue;
            }
            $directory = ROOT . '/' . $value;
            if (false === is_dir($directory)) {
                continue;
            }
            if ($directory === self::APP_COMMON_PATH) {
                continue;
            }
            //开始 方便查看配置以及验证： 端口是否重复？绑定的域名是啥？
            $iniFile = "{$directory}/{$configDirectoryName}/{$iniFileName}";
            if(true === file_exists($iniFile)){
                $iniConfig = parse_ini_file($iniFile,true);
                $binds = getItemFromArray($iniConfig,'bind',[]);
                $itsHost = getItemFromArray($binds,'host','未配置域名');
                $itsPort = getItemFromArray($binds,'port','未配置端口');
                unset($iniConfig['bind']);
            }
            else{
                $iniConfig = [];
                $itsHost = '';
                $itsPort = 0;
            }
            $v = str_replace($prefix, '', $value);
            $menuArray[] = "{$v}--{$itsHost}:{$itsPort}";
            //结束
            $hosts[$v] = [
                'directory' => $value,
                'host' => $itsHost,
                'port' => intval($itsPort),
                'swooleConfig' => $iniConfig,
            ];
        }
        $str = implode(PHP_EOL, $menuArray);
        //
        $cliParams = getopt('h:',[
            'host:',
        ]);
        $specifiedHost = getItemFromArray($cliParams,'h','');
        if('' === $specifiedHost || false === $specifiedHost){
            $specifiedHost = getItemFromArray($cliParams,'host','');
        }
        unset($cliParams);
        //
        if('' === $specifiedHost || false === $specifiedHost){
            echo '选项:'.PHP_EOL.$str.PHP_EOL;
            echo "请输入要绑定的域名: ".PHP_EOL;
            $specifiedHost = fgets(STDIN);
            $specifiedHost = trim($specifiedHost);
        }
        $itsConfig = getItemFromArray($hosts, $specifiedHost, []);
        if(true === empty($itsConfig)){
            exit('必须绑定域名'.PHP_EOL);
        }
        define('APP_DIRECTORY_NAME',$itsConfig['directory']);
        define('APP_PATH', ROOT.'/'.APP_DIRECTORY_NAME);
        unset($itsConfig['directory']);
        return $itsConfig;
    }

    private function buildHttpServerConfig(bool $isHttpServer, array &$iniConfig, string $host): array
    {
        $configDirectoryName = $this->swooleConfigDirectoryName;
        $basicConfig = require ROOT . '/must/'.$configDirectoryName.'/serverBasicConfig.php';
        //
        $file = true === $isHttpServer ? 'httpServerConfig.php' : 'webSocketServerConfig.php';
        $personalConfig = require APP_PATH . '/'.$configDirectoryName.'/'.$file;
        //
        $iniConfig = $this->buildIniConfig($isHttpServer, $iniConfig, $host);
        return array_merge($basicConfig, $iniConfig, $personalConfig);
    }

    private function buildIniConfig(bool $isHttpServer, array &$iniConfig, string $host): array
    {
        $multiples = getItemFromArray($iniConfig,'multiple',[]);
        $mainMultiple = getItemFromArray($multiples,'workerMultiple', 1);
        $mainMultiple = intval($mainMultiple);
        $taskMultiple = getItemFromArray($multiples,'taskWorkerMultiple', 1);
        $taskMultiple = intval($taskMultiple);
        unset($iniConfig['multiple']);
        //
        $logDirectory = true === $isHttpServer ? 'swooleHttpLogs' : 'swooleWebSocketLogs';
        $pidFileName = true === $isHttpServer ? 'httpServer.pid' : 'webSocketServer.pid';
        $directory = APP_PATH;
        $sslPath = true === IS_LOCAL ? ROOT.'/must' : $directory;
        $sslPath.= '/ssl';
        $cpuNum = swoole_cpu_num();
        $config = [
            'reload_async' => true,
            /*设置启动的Worker进程数。
            业务代码是全异步非阻塞的，这里设置为CPU核数的1-4倍最合理
            业务代码为同步阻塞，需要根据请求响应时间和系统负载来调整，例如：100-500
            默认设置为SWOOLE_CPU_NUM，最大不得超过SWOOLE_CPU_NUM * 1000*/
            'worker_num' =>  $cpuNum * $mainMultiple,
            /* 最大值不得超过 SWOOLE_CPU_NUM * 1000*/
            'task_worker_num' => $cpuNum * $taskMultiple,
            'pid_file' => "{$directory}/data/pid/{$pidFileName}",
            'ssl_cert_file' => "{$sslPath}/{$host}.crt",
            'ssl_key_file'  => "{$sslPath}/{$host}.key",
            'log_file' => "{$directory}/data/{$logDirectory}/swoole_log.txt",
            'log_level' => true === IS_LOCAL ? SWOOLE_LOG_NONE : SWOOLE_LOG_WARNING | SWOOLE_LOG_ERROR,
            'log_rotation' => SWOOLE_LOG_ROTATION_DAILY,//日志分割
            'log_date_format' => '%Y-%m-%d %H:%M:%S',
            'trace_flags' => SWOOLE_TRACE_MYSQL_CLIENT,
        ];
        $stringConfig = getItemFromArray($iniConfig,'string',[]);
        $booleanConfig = getItemFromArray($iniConfig,'boolean',[]);
        foreach ($booleanConfig as &$value){
            $value = boolval($value);
        }
        $integerConfig = getItemFromArray($iniConfig,'integer',[]);
        foreach ($integerConfig as &$value){
            $value = intval($value);
        }
        return array_merge($config, $stringConfig, $integerConfig, $booleanConfig);
    }

    /**
     *  worker启动后，根据配置文件定义项目配置
     * @return void
     */
    public static function defineApplicationConfig()
    {
        $finalConfig = self::buildApplicationConfig();
        define('APP_CONFIG', $finalConfig);
    }

    private static function buildApplicationConfig(): array
    {
        $businessConfig = require self::APP_COMMON_PATH . '/config/business.php';
        $file = APP_PATH.'/config/business.php';
        $siteSpecialConfig = true === file_exists($file) ? require $file : [];
        return array_merge($businessConfig,$siteSpecialConfig);
    }

    public static function requireFunctions($functionsPath): bool
    {
        if( false === is_dir($functionsPath))
        {
            $br = PHP_EOL;
            $file = __FILE__;
            $line = __LINE__ + 1;
            echo "这个目录不存在:{$functionsPath}, {$br}追溯：{$file}第{$line}行{$br}";
            return false;
        }
        $files = scandir($functionsPath);
        unset($files[0],$files[1]);
        foreach($files as $file)
        {
            require $functionsPath.'/'.$file;
        }
        return true;
    }

    /**
     * 关闭上次的服务
     * @param string $pidFile
     * @return bool
     */
    public function kill(string $pidFile): bool
    {
        if(false === file_exists($pidFile)){
            return false;
        }
        $pid = file_get_contents($pidFile);
        $pid = intval($pid);
        if($pid === 0){
            return false;
        }
        echo '正在关闭上次的服务--pid='.$pid.PHP_EOL;
        exec("kill -15 {$pid}");
        sleep(2);
        echo '关闭上次的服务结束'.PHP_EOL;
        return true;
    }

    private function flushRedisDB(){
        echo '即将flush redisDB'.PHP_EOL;
        $config = self::buildApplicationConfig();
        say('$config',$config);
        $redis = new \Redis();
        $redisConfig = &$config['redis'];
        $redis->connect($redisConfig['host'],$redisConfig['port']);
        $redis->auth($redisConfig['password']);
        $redis->select($config['debug']['redis']['dbIndex']);
        $redis->flushDB();
        echo 'flush redisDB结束'.PHP_EOL.PHP_EOL;
        unset($redis);
    }
}


