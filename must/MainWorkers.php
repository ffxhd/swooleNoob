<?php
namespace must;
use app_common\PC;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;

/** 发起http请求 $fd = 1， 服务端认为请求结束后，刷新，再次发起http请求$fd = 2,（一个浏览器，同一个窗口）
dispatch 调度 Dispatcher 调度员
 */
class MainWorkers extends WorkersCommon
{

    public function onRequest(Request $request, Response $response)
    {
        /*注意：//Fatal error: Uncaught Exception: Serialization of 'Swoole\Http\Request' is not allowed*/

        //return $response->end('hello world');
        /*if(true === IS_LOCAL){
            say('onRequest--$request',$request,
                'rawContent',$request->getContent());
        }*/
        /*onRequest--$request =>[object] Swoole\Http\Request::__set_state(array(
   'fd' => 1,
   'streamId' => 0,
   'header' => array (
        'host' => 'dev.roc-fly.top:9501',
        'connection' => 'keep-alive',
        'content-length' => '245',
        'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36',
        'x-requested-with' => 'XMLHttpRequest',
        'authorization' => 'null',
        'content-type' => 'multipart/form-data; boundary=----WebKitFormBoundaryTAI2WAYREkGvSQXS',
        'accept' => '',
        'origin' => 'http://html.chess.local',
        'sec-fetch-site' => 'cross-site',
        'sec-fetch-mode' => 'cors',
        'sec-fetch-dest' => 'empty',
        'referer' => 'http://html.chess.local/login.html',
        'accept-encoding' => 'gzip, deflate, br',
        'accept-language' => 'zh-CN,zh;q=0.9',
    ),
   'server' => array (
      'request_method' => 'POST',
      'request_uri' => '/api/player/login',
      'path_info' => '/api/player/login',
      'request_time' => 1587178488,
      'request_time_float' => 1587178488.620094,
      'server_protocol' => 'HTTP/1.1',
      'server_port' => 9501,
      'remote_port' => 60479,
      'remote_addr' => '192.168.56.103',
      'master_time' => 1587178487,
    ),
   ‘cookie' =>
      array (
        'PHPSESSID' => 'fd_7_15893375825ebb5dee87844',
    ),
   'get' => NULL,
   'files' => NULL,
   'post' => array (
      'userName' => 'ffxhd',
      'password' => 'rgerye',
    ),
   'tmpfiles' => NULL,
))*/
        //say('onRequest--$request',$request,'$response',$response);
        $uri = $request->server['request_uri'];
        if ($uri === '/favicon.ico')
        {
            $response->sendfile(APP_CONFIG['favicon_ico']);
            return false;
        }
        $origin = getItemFromArray($request->header,'origin','*');
        $response->header('Access-Control-Allow-Origin', $origin);
        unset($origin);
        $response->header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');//, PUT,OPTIONS
        $response->header('Access-Control-Allow-Credentials', 'true');//配合ajax中  xhr.withCredentials = true;
        //Origin, X-Requested-With, Content-Type, Accept, Authorization
        $response->header('Access-Control-Allow-Headers', 'Origin,X-Requested-With,Content-Type,Accept,Authorization,Cookie');
        /*The Access-Control-Max-Age 这个响应头表示 preflight request  （预检请求）的返回结果
        （即 Access-Control-Allow-Methods 和Access-Control-Allow-Headers 提供的信息） 可以被缓存多久。
        返回结果可以用于缓存的最长时间，单位是秒。在Firefox中，上限是24小时 （即86400秒），
        而在Chromium 中则是10分钟（即600秒）。Chromium 同时规定了一个默认值 5 秒。
        如果值为 -1，则表示禁用缓存，每一次请求都需要提供预检请求，即用OPTIONS请求进行检测。*/
        $response->header('Access-Control-Max-Age', 600);
        if( $request->server['request_method'] === 'OPTIONS')
        {
            $response->status(200);
            return true;
        }
        $response->header('Content-Type', 'text/html; charset=utf-8');
        if( true === IS_LOCAL)
        {
            $response->header('worker-id', $this->workerId);
        }
        //避免使用超全局变量
        unset($_POST, $_GET, $_COOKIE, $_REQUEST, $_SESSION, $_FILES, $_SERVER);
        //执行任务---找到模块、控制器、方法
        $mca = PC::analysisUri($uri);
        if( true === is_string($mca)) {
            $response->end($mca);
            return true;
        }
        //分别判断模块、控制器、方法是否存在
        $fd = $request->fd;
        $files = $request->files;//注意 当 $request 对象销毁时，会自动删除上传的临时文件
        $requestData = [
            'fd'     => $fd,//为了构造SESSION的键
            'header' => $request->header,
            'server' => $request->server,
            'cookie' => $request->cookie,
            'get'    => $request->get,
            'post'   => $request->post,
            'files'  => true === is_array($files) ? $files : [],
            'tmpFiles'   => $request->tmpfiles,
            'rawContent' => $request->getContent(),
        ];
        $content = PC::run($mca, $requestData);
        $response->end($content);
        return true;
    }

    public function onFinish(Server $server, int $taskId, $data)
    {
        //say('onFinish--$data',$data);
    }

    public function onPipeMessage(Server $server, int $src_worker_id, $data)
    {

    }
}