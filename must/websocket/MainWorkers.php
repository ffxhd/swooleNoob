<?php
namespace must\websocket;
use JetBrains\PhpStorm\Pure;
use Swoole\Http\Request;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;

class MainWorkers
{
    private object $controller;
    #[Pure] public function __construct(){
        if( 0 > 1){
            $this->controller = new \app_chess\socket\controller\MainWorkers();
        }
    }

    public function initializeController(string $mainClass){
        $this->controller = new $mainClass();
    }
    
    public function onOpen(Server $ws, Request $request){
        $this->controller->onOpen($ws, $request);
    }

    public function onClose(Server $ws, int $fd){
        $this->controller->onClose($ws, $fd);
    }

    /**
     * @throws \Exception
     */
    public function onMessage(Server $ws, Frame $frame){
        $this->controller->onMessage($ws, $frame);
    }

    public function onFinish(Server $server, int $taskId, $data){
        $this->controller->onFinish($server, $taskId, $data);
    }

    public function onPipeMessage(Server $server, int $src_worker_id, $data){
        $this->controller->onPipeMessage($server, $src_worker_id, $data);
    }
}